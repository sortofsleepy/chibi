Chibi
===

A tiny Vulkan graphics framework built as an experiment. Planned experimental support for WebGPU via Google's ["Dawn"](https://dawn.googlesource.com/dawn) project

Requirements
===

* [CMake](https://cmake.org/)
* You will need Boost, as well but just build the system and filesystem libraries. 
* [GLFW](https://www.glfw.org/)
* [GLM](https://glm.g-truc.net/0.9.9/index.html)
* [STB](https://github.com/nothings/stb)
* [MinGW](http://www.mingw.org/) - this is technically optional, I just mention it here cause I'm on Windows. 


Dawn
====
If you're on Windows, note that you'll likely have to use Visual Studio for any Dawn related development, I could not find a way to build a MinGW compatible version of the library.
Otherwise, CLion or other editors should be fine(You can technically do Dawn stuff in CLion as well but you won't have debugging support)


Shaderc 
=====
You will also need Google's shaderc project in order to compile SPIR-V modules from GLSL / HLSL code. The build process is somewhat complicated hence it's
own section. Assuming you're using CMake,

* clone [shaderc](https://github.com/google/shaderc)
* Within the repo, there is a folder called `third_party`.
* Clone [glslang](https://github.com/KhronosGroup/glslang) and [SPIRV-Tools](https://github.com/KhronosGroup/SPIRV-Tools)
* Within SPIRV-Tools, there is a folder called `external`, within that folder clone [SPIRV-Headers](https://github.com/KhronosGroup/SPIRV-Headers)
* Now you can go through the normal cmake process. Pass in `-DCSHADERC_SKIP_TESTS=true` if you don't want to run tests, otherwise you need to install [Google Test](https://github.com/google/googletest). 
* It's advised to build in Release; the debug build has quite a bit of extra code that makes it difficult to use during development(at least on my system)



Credits
===
* Too many to state - but all licences and notices have been left if available.
* A lot of the Vulkan code is based on [Alexander Overvoorde's excellent tutorial series](https://vulkan-tutorial.com)