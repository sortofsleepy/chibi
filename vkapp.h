//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_VKAPP_H
#define CHIBI_VKAPP_H

#include <vector>

#include "chibi/core/appbase.h"
#include "chibi/core/appwindow.h"

#include "glm/mat4x4.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "chibi/file.h"
#include "chibi/core/vk/vkrenderer.h"
#include "chibi/framework/mesh.h"
#include "chibi/core/shader.h"
#include "chibi/core/vk/buffer.h"
#include "chibi/core/vk/ubo.h"

#define STRINGIFY(A) #A

using namespace chibi;
using namespace chibi::core;
using namespace glm;
using namespace chibi::framework;

// basic mat4s that need to go to the shader.
typedef struct {
    glm::mat4 projectionMatrix,viewMatrix,model;
} Camera;

class VkApp : public chibi::core::AppBase {

    // renderer
    chibi::vk::VkRendererRef renderer;

    // Mesh object to render
    MeshRef mesh;

    // Shader to use in rendering
    ShaderRef shader;

    BufferRef vertexData;

    Camera cam;
    chibi::vk::UboRef ubo;
public:


    void setup() override{
        // setup the renderer
        renderer = VkRenderer::create();
        renderer->setup();

        // build the format for the shader and create shader.
        Shader::ShaderFormat format;
        format.vertex("shaders/vertex.glsl")
        .fragment("shaders/fragment.glsl");
        shader = Shader::create(format, renderer->getLogicalDevice());

        //////// BUILD UBO ////////////
        ubo = Ubo::create(renderer);
        ubo->init(sizeof(cam));

        /////// HANDLE MESH //////////////////

        std::vector<glm::vec4> verts = {
                vec4(-0.5f, -0.5f,1.0,1.),
                vec4(0.5f, -0.5f, 1.0f,1.),
                vec4(0.5f, 0.5f, 1.0f,1.),
                vec4(-0.5f, 0.5f,1.0,1.0)
        };


        std::vector<glm::vec3> colors = {
                vec3(1.0f, 1.0f, 1.0f),
                vec3(0.0f, 1.0f, 0.0f),
                vec3(0.0f, 0.0f, 1.0f),
                vec3(1.0f, 0.0f, 1.0f)
        };

        std::vector<uint16_t> indices = {
                0,1,2,2,3,0
        };

        // construct the pipeline for the mesh
        mesh = Mesh::create(renderer,shader);

        mesh->addData(verts,"position");
        mesh->addData(colors,"colors");

        mesh->addIndices(indices);


    }
    void draw() override{

        renderer->drawFrame();
        mesh->draw();

    }

};
#endif //CHIBI_VKAPP_H
