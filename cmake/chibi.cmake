
function(chibi_app)
    set( oneValueArgs
        APP
        # the path to Cinder installation
        CHIBI_PATH

        # the type of platform to compile fore
        PLATFORM

        VERBOSE_MAKE

        # whether or not you need Vulkan to list all found extensions
        VULKAN_LIST_EXTENSIONS

        # enables verbose logging of vulkan instances
        VULKAN_VERBOSE
        )
    set( multiValueArgs

        # Sepecifies the type of build you want to do, either "Debug" or "Release"
        BUILD_TYPE

        # argument to tell emscripten to bundle resources
        RESOURCES

        # argument to tell what to name all the files it will output
        OUTPUT_NAME

        # argument to tell Emscripten where to write all compiled files.
        OUTPUT_DIRECTORY
        )

    cmake_parse_arguments( ARG "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )


    # set C++ standard. Important cause otherwise build will fail.
    set (CMAKE_CXX_STANDARD 17)

    if( ARG_UNPARSED_ARGUMENTS )
        message( WARNING "unhandled arguments: ${ARG_UNPARSED_ARGUMENTS}" )
    endif()

    if(ARG_VERBOSE_MAKE)
        set( CMAKE_VERBOSE_MAKEFILE on )
    endif()


    # if build type isn't set - set things to "Debug"
    if (NOT ARG_BUILD_TYPE)
        set(CMAKE_BUILD_TYPE "Debug")
        add_definitions(-DCHIBI_DEBUG)
        add_definitions(-DDEBUG)
    endif ()

    # otherwise set build type to whatever was passed in. Note that you can only pass in "Debug" or "Release"
    # not the most elegant of methods but does seem to work.
    if (ARG_BUILD_TYPE)
        if(${ARG_BUILD_TYPE} MATCHES "Debug")
            set(CMAKE_BUILD_TYPE "Debug")
            add_definitions(-DCHIBI_DEBUG)
            add_definitions(-DDEBUG)
            add_compile_definitions(DEBUG=TRUE)
        elseif(${ARG_BUILD_TYPE} MATCHES "Release")
            set(CMAKE_BUILD_TYPE "Release")
            add_definitions(-DCHIBI_RELEASE)
            add_compile_definitions(RELEASE=TRUE)
        else()
            message(STATUS "Build type can only be set to \"Debug\" or \"Release\" - settings things to \"Debug\"")
        endif()
    endif ()

    # set runtime output directory when doing Debug builds
    if("Debug" STREQUAL "${CMAKE_BUILD_TYPE}" )
        set( CMAKE_RUNTIME_OUTPUT_DIRECTORY "Debug" )
    endif()

    # Complete build setup based on settings
    include(cmake/vulkan.cmake)
   

    # copy shader files to output directory.
    add_custom_command(TARGET index POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/shaders ${CMAKE_CURRENT_SOURCE_DIR}/cmake-build-debug/Debug/shaders)

endfunction()