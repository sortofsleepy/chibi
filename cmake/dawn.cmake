

# add "define" statement so we can properly query in source for vulkan
add_definitions(-DCHIBI_DAWN)
add_definitions(-DPLATFORM_DAWN)



# add initial settings
add_executable(index
    ${ARG_APP}

    ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/shader.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/file.cpp
    )


target_include_directories(
    index
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/code/include
    PUBLIC ${CMAKE_SOURCE_DIR}/code/libraries/shaderrc/include
    PUBLIC ${CMAKE_SOURCE_DIR}/code/libraries/boost/include
)

target_link_libraries(index
    ${CMAKE_SOURCE_DIR}/code/libraries/boost/lib/filesystem.a
    ${CMAKE_SOURCE_DIR}/code/libraries/boost/lib/system.a
    ${CMAKE_SOURCE_DIR}/code/libraries/shaderrc/lib/libshaderc_combined.a
    )

