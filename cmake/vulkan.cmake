
############ SETUP VULKAN REQUIRED THINGS ############

# looks to ensure Vulkan sdk in installed on the system.
find_package(Vulkan REQUIRED)
if (NOT Vulkan_FOUND)
  message(SEND_ERROR "Vulkan was not found on this system.")
endif ()


# add "define" statement so we can properly query in source for vulkan
add_definitions(-DGLFW_INCLUDE_VULKAN)
add_definitions(-DCHIBI_VULKAN)
add_definitions(-DPLATFORM_VULKAN)

  # add initial settings
  add_executable(index
      ${ARG_APP}
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/vkrenderer.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/validator.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/swapchain.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/shader.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/file.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/framework/mesh.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/buffer.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/ubo.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/code/src/core/vk/dynamicubo.cpp
      )

target_compile_definitions(index PRIVATE VK_USE_PLATFORM_WIN32_KHR)

  target_include_directories(
      index
      PUBLIC ${Vulkan_INCLUDE_DIR}
      PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/code/include
      PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/code/libraries/glfw/include
      PUBLIC ${CMAKE_SOURCE_DIR}/code/libraries/shaderrc/include
      PUBLIC ${CMAKE_SOURCE_DIR}/code/libraries/boost/include
      PUBLIC ${CMAKE_SOURCE_DIR}/code/libraries/glm
  )

  target_link_libraries(index
      "${CMAKE_SOURCE_DIR}/code/libraries/glfw/lib/libglfw3.a"
      "${Vulkan_LIBRARY}"
      ${CMAKE_SOURCE_DIR}/code/libraries/boost/lib/filesystem.a
      ${CMAKE_SOURCE_DIR}/code/libraries/boost/lib/system.a
      ${CMAKE_SOURCE_DIR}/code/libraries/shaderrc/lib/libshaderc_combined.a
      )

