#include "chibi/core/appwindow.h"
#ifdef PLATFORM_VULKAN
#include "vkapp.h"
    #include "chibi/core/vk/vkrenderer.h"
#endif

using namespace chibi;

int main()
{

    // setup window - window is singleton
    WindowRef window = AppWindow::getInstance();
    window->setup(1024,768,"Test Window");

    // create a new app instance
#ifdef PLATFORM_VULKAN
    auto * app = new VkApp();
    app->setup();
    app->run();
#endif


    return 0;

}
