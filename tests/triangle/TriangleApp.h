//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_VKAPP_H
#define CHIBI_VKAPP_H

#include "chibi/core/appbase.h"
#include "chibi/core/appwindow.h"

#include "chibi/file.h"
#include "chibi/core/vk/vkrenderer.h"
#include "chibi/framework/mesh.h"
#include "chibi/core/shader.h"

#define STRINGIFY(A) #A

using namespace chibi;
using namespace chibi::core;

// renders a triangle. Assumes vertices are specified in-shade.r
class TriangleApp : public chibi::core::AppBase {

    // renderer
    chibi::vk::VkRenderer renderer;

    // Mesh object to render
    chibi::framework::Mesh mesh;

    // Shader to use in rendering
    ShaderRef shader;
public:


    void setup() override{

        // setup the renderer
        renderer.setup();

        // build the format for the shader and create shader.
        Shader::ShaderFormat format;
        format.vertex("shaders/vertex.glsl")
                .fragment("shaders/fragment.glsl");
        shader = Shader::create(format, renderer.getLogicalDevice());

        // construct the pipeline for the mesh
        mesh.buildGraphicsPipeline(renderer.getLogicalDevice(),shader,renderer.getSwapChain());

        // setup framebuffers for the mesh's renderpass
        renderer.setupFramebuffers(mesh.renderPass);

    }

    void draw() override{
        // record the image
        renderer.record(mesh.graphicsPipeline);

        // render the frame
        renderer.drawFrame();
    }

};
#endif //CHIBI_VKAPP_H
