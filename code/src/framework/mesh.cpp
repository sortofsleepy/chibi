//
// Created by sortofsleepy on 6/12/2020.
//

#include "chibi/framework/mesh.h"
#include "chibi/core/appwindow.h"


using namespace chibi;
using namespace std;

namespace chibi::framework{


    void Mesh::buildRenderPass(VkDevice device, SwapChainRef swapchain) {

        colorAttachment.format = swapchain->getImageFormat();
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;

        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = 1;
        renderPassInfo.pAttachments = &colorAttachment;
        renderPassInfo.subpassCount = 1;
        renderPassInfo.pSubpasses = &subpass;
        renderPassInfo.dependencyCount = 1;
        renderPassInfo.pDependencies = &dependency;

        if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
            throw std::runtime_error("failed to create render pass!");
        }
    }

    void Mesh::buildGraphicsPipeline(chibi::vk::SwapChainRef swapchain) {
        if(swapchain == nullptr){
            swapchain = renderer->getSwapChain();
        }

        // if mesh is set up to adjust according to window size, tweak accordingly.
        if(resizeToWindow){
            viewWidth = AppWindow::getInstance()->getWidth();
            viewHeight = AppWindow::getInstance()->getHeight();
        }

        buildRenderPass(*renderer->getLogicalDevice(),swapchain);

        // compile mesh data.
        //_compileMeshData(renderer->getLogicalDevice());

        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;


        if(!attribDescriptions.empty() && !bindingDescriptions.empty()){

            vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t >(bindingDescriptions.size());
            vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();

            vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t >(attribDescriptions.size());
            vertexInputInfo.pVertexAttributeDescriptions = attribDescriptions.data();

        }


        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = primitiveType;
        inputAssembly.primitiveRestartEnable = primitiveRestart;


        viewport.x = viewX;
        viewport.y = viewY;
        viewport.width = (float) viewWidth;
        viewport.height = (float) viewHeight;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;


        VkExtent2D extent;
        extent.width = (float) viewWidth;
        extent.height = (float) viewHeight;
        scissor.offset = {0, 0};
        scissor.extent = extent;


        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = viewportCount;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = scissorCount;
        viewportState.pScissors = &scissor;

        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE;


        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        colorBlendAttachment.blendEnable = VK_FALSE;


        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.logicOp = VK_LOGIC_OP_COPY;
        colorBlending.attachmentCount = 1;
        colorBlending.pAttachments = &colorBlendAttachment;
        colorBlending.blendConstants[0] = 0.0f;
        colorBlending.blendConstants[1] = 0.0f;
        colorBlending.blendConstants[2] = 0.0f;
        colorBlending.blendConstants[3] = 0.0f;


        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 0;
        pipelineLayoutInfo.pushConstantRangeCount = 0;

        if (vkCreatePipelineLayout(*renderer->getLogicalDevice(), &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layout!");
        }

        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = shader->getShaderInfo()->size();
        pipelineInfo.pStages = shader->getShaderInfo()->data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssembly;
        pipelineInfo.pViewportState = &viewportState;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.layout = pipelineLayout;
        pipelineInfo.renderPass = renderPass;
        pipelineInfo.subpass = 0;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

        VkResult res = vkCreateGraphicsPipelines(*renderer->getLogicalDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline);
        if (res != VK_SUCCESS){
            LOG_VULKAN(res);
            throw std::runtime_error("failed to create graphics pipeline!");
        }
    }

    void Mesh::_rebuildPipeline() {
        vkDestroyPipeline(*renderer->getLogicalDevice(), graphicsPipeline, nullptr);
        vkDestroyPipelineLayout(*renderer->getLogicalDevice(), pipelineLayout, nullptr);
        buildGraphicsPipeline(renderer->getSwapChain());
    }


    void Mesh::draw(){

        // if the pipeline hasn' been built yet, build it.
        if(!pipelineBuilt){
            buildGraphicsPipeline();
            pipelineBuilt = true;
        }


        // TODO this probably still needs some work to get a more flexible command system.
        command.firstBinding = 0;
        command.bindingCount = 1;
        command.buffers = meshBuffers;
        command.offsets = readOffsets;

        command.vertexCount = static_cast<uint32_t>(3);
        command.instanceCount = 1;
        command.firstVertex = 0;
        command.firstInstance = 0;
        command.pipeline = graphicsPipeline;


        if(indexBuffer != nullptr){
            command.indexType = indexBuffer->getType();
            command.indexBuffer = indexBuffer->getBuffer();
            command.numIndices = indexBuffer->getNumIndices();
        }

        renderer->recordMultiple(command);

    }

    void Mesh::_defaultSettings() {
        WindowRef window = AppWindow::getInstance();

        resizeToWindow = true;
        viewWidth = (float)window->getWidth();
        viewHeight = (float)window->getHeight();
        viewX = 0.0f;
        viewY = 0.0f;

        primitiveType = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        primitiveRestart = VK_FALSE;

        pipelineBuilt = false;

        viewportCount = 1;
        scissorCount = 1;

        // setup resize listeners
        renderer->addResizeHandler(std::bind(&Mesh::_rebuildPipeline,this));
    }


    template<typename T>
    Mesh& Mesh::addData(std::vector<T> data, std::string name) {

        auto buffer = Buffer::create();

        // if no name is passed in, just take the current number of attributes which should
        // make the data align with the correct lookup index in the mapping.
        if(name.empty()){
            name = std::to_string(attributes.size());
        }

        // if attributes aren't empty, set up binding locations and attribute
        // bindings accordingly.
        // The mesh is currently intended for non-interleaved data.
        if(!attributes.empty()){
            buffer->setBindingLocation(attributes.size());
            buffer->setAttributeLocation(attributes.size());
            buffer->setAttributeBinding(attributes.size());
        }

        // Compile the data into the buffer
        buffer->setData(renderer,data);

        // push back another description set and binding set.
        attribDescriptions.push_back(buffer->getAttributeDescription());
        bindingDescriptions.push_back(buffer->getBindingDescription());

        meshBuffers.push_back(buffer->getBuffer());
        readOffsets.push_back(0);

        attributes.insert(std::pair(name,buffer));


        return *this;
    }

    template<typename A>
    void Mesh::addIndices(std::vector<A> indices) {
        indexBuffer = IndexBuffer::create();
        indexBuffer->loadData(renderer,indices);
        command.numIndices = indices.size();
        command.indexType = indexBuffer->getType();
        command.indexBuffer = indexBuffer->getBuffer();
    }

    template Mesh& Mesh::addData(std::vector<glm::vec4> data, std::string name);
    template Mesh& Mesh::addData(std::vector<glm::vec3> data, std::string name);
    template Mesh& Mesh::addData(std::vector<glm::vec2> data, std::string name);
    template Mesh& Mesh::addData(std::vector<float> data, std::string name);
    template Mesh& Mesh::addData(std::vector<int> data, std::string name);

    template void Mesh::addIndices(std::vector<uint16_t> indices);
    template void Mesh::addIndices(std::vector<uint32_t> indices);

} // end namespac1