//
// Created by sortofsleepy on 1/18/2020.
//

#include "chibi/core/shader.h"

using namespace std;
using namespace chibi::core;
namespace chibi::vk {

    Shader::Shader(ShaderFormat format){
        this->format = format;
    }

    Shader::Shader(ShaderFormat format, VkDevice * device){
        this->format = format;
        generateShaderModules(device);
    }

    void Shader::generateShaderModules(VkDevice *device) {

        // check which parts need to get compiled and compile
        if(format.vertexSet()){
            buildVertexShader(device);
        }

        if(format.fragmentSet()){
            buildFragmentShader(device);
        }

    }

    void Shader::buildVertexShader(VkDevice *device) {

        std::string src = "";
        if(isShaderSource(format._vertex)){
            src = format._vertex;
        }else{
            src = File::loadTextFile(format._vertex);
        }

        VkPipelineShaderStageCreateInfo vertex = {};
        compile(device,"vertex",shaderc_vertex_shader,src,true);
        vertex.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertex.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertex.module = vertexModule;
        vertex.pName = "main";

        shaderInfo.push_back(vertex);
    }

    void Shader::buildFragmentShader(VkDevice *device) {


        std::string src = "";
        if(isShaderSource(format._fragment)){
            src = format._fragment;
        }else{
            src = File::loadTextFile(format._fragment);
        }

        VkPipelineShaderStageCreateInfo fragment = {};
        compile(device,"fragment",shaderc_fragment_shader,src,true);
        fragment.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragment.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragment.module = fragmentModule;
        fragment.pName = "main";

        shaderInfo.push_back(fragment);
    }


    bool Shader::isShaderSource(std::string srcOrPath) {
        size_t pos = srcOrPath.find("#version");
        return pos != std::string::npos;
    }

    void Shader::compile(VkDevice * device,const std::string source_name, shaderc_shader_kind kind, const std::string source,
                         bool optimize) {

        shaderc::Compiler compiler;
        shaderc::CompileOptions options;
        if(optimize){
            options.SetOptimizationLevel(shaderc_optimization_level_size);

        }

        shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(source,kind,source_name.c_str(),options);
        if(module.GetCompilationStatus() != shaderc_compilation_status_success){
            throw std::runtime_error("Error compiling module - " + module.GetErrorMessage());
        }

        std::vector<uint32_t> spirv = {module.cbegin(),module.cend()};

        // build vulkan module
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = spirv.size() * sizeof(unsigned int);
        createInfo.pCode = spirv.data();

        switch(kind){
            case shaderc_vertex_shader:

                if(vkCreateShaderModule(*device,&createInfo,nullptr,&vertexModule) != VK_SUCCESS){
                    throw std::runtime_error("Error creating vertex module");
                }
                break;

            case shaderc_fragment_shader:
                if(vkCreateShaderModule(*device,&createInfo,nullptr,&fragmentModule) != VK_SUCCESS){
                    throw std::runtime_error("Error creating fragment module");
                }
                break;

                // TODO compute + geometry
        }



    }
}