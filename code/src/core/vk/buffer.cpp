//
// Created by sortofsleepy on 7/16/2020.
//

#include <chibi/log.h>
#include "chibi/core/vk/buffer.h"

using namespace std;
using namespace glm;
using namespace chibi;

namespace chibi::vk {
    Buffer::Buffer():
    descriptionOffset(0),
    bindingDescription({}),
    bufferUsage(VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT),
    memFlags(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
    inputRate(VK_VERTEX_INPUT_RATE_VERTEX),
    format(VK_FORMAT_R32G32B32_SFLOAT){

        attributeDescription.binding = 0;
        attributeDescription.location = 0;
        attributeDescription.format = format;
        attributeDescription.offset = descriptionOffset;

        bindingDescription.binding = 0;
        bindingDescription.stride = 0;
        bindingDescription.inputRate = inputRate;
    }


    template<typename T>
    void Buffer::setData(vk::VkRendererRef renderer, std::vector<T> _data) {
        auto device = *renderer->getLogicalDevice();

        if(std::is_same<T,glm::vec4>::value){
            // change format to appropriate value
            format = VK_FORMAT_R32G32B32A32_SFLOAT;
            attributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
        }else if(std::is_same<T,glm::vec2>::value){
            // change format to appropriate value
            format = VK_FORMAT_R32G32_SFLOAT;
            attributeDescription.format = VK_FORMAT_R32G32_SFLOAT;
        }else if(std::is_same<T,float>::value){
            format = VK_FORMAT_R32_SFLOAT;
            attributeDescription.format = VK_FORMAT_R32_SFLOAT;
        }else if(std::is_same<T,int>::value){
            format = VK_FORMAT_R32_SINT;
            attributeDescription.format = VK_FORMAT_R32_SINT;
        }

        //setup stride depending on data type for buffer
        bindingDescription.stride = sizeof(T);
        bufferSize = sizeof(_data[0]) * _data.size();

        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        generateBuffer(renderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

        // copy data over onto staging buffer
        void* data;
        vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
        memcpy(data, _data.data(), (size_t) bufferSize);
        vkUnmapMemory(device, stagingBufferMemory);

        // generate main buffer to use
        generateBuffer(renderer,bufferSize, bufferUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer,bufferMemory);

        // copy info from staging to main buffer.
        bufferutils::copyBuffer(device,*renderer->getGraphicsQueue(),*renderer->getRendererCommandPool(),stagingBuffer,buffer,bufferSize);

        // clean up the staging buffer and memory
        vkDestroyBuffer(*renderer->getLogicalDevice(),stagingBuffer,nullptr);
        vkFreeMemory(*renderer->getLogicalDevice(),stagingBufferMemory,nullptr);
    }

    void Buffer::generateBuffer(vk::VkRendererRef renderer,VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {

        auto device = *renderer->getLogicalDevice();

        VkBufferCreateInfo bufferInfo{};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size = size;
        bufferInfo.usage = usage;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
            throw std::runtime_error("failed to create buffer!");
        }

        VkMemoryRequirements memRequirements;
        vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

        VkMemoryAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = renderer->getMemoryPropertyFlag(memRequirements.memoryTypeBits, properties);

        if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate buffer memory!");
        }

        vkBindBufferMemory(device, buffer, bufferMemory, 0);
    }

    void Buffer::generateBuffer(vk::VkRendererRef renderer) {
        generateBuffer(renderer,bufferSize,usage,memoryPropertyFlags,buffer,bufferMemory);
    }
#ifdef CHIBI_DEBUG

    void Buffer::logData(std::string name) {
        LOG_I("Logging values for : " << name);

        LOG_I("BindingDescription values: ");
        LOG_I("Binding : " << bindingDescription.binding);
        LOG_I("Stride: " << bindingDescription.stride);
        if(bindingDescription.inputRate == VK_VERTEX_INPUT_RATE_VERTEX){
            LOG_I("Input rate is correct. \n\n");
        }


        LOG_I("Attrib description values: ");
        LOG_I("Binding :" << attributeDescription.binding);
        LOG_I("Location :" << attributeDescription.location);
        LOG_I("offset : " << attributeDescription.offset);

    }
#endif


template void Buffer::setData(chibi::vk::VkRendererRef renderer, ::vector<glm::vec4> data);
template void Buffer::setData(chibi::vk::VkRendererRef renderer, ::vector<glm::vec3> data);
template void Buffer::setData(chibi::vk::VkRendererRef renderer, ::vector<glm::vec2> data);
template void Buffer::setData(chibi::vk::VkRendererRef renderer, ::vector<float> data);
template void Buffer::setData(chibi::vk::VkRendererRef renderer, ::vector<int> data);
}// end namespace
