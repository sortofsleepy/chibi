//
// Created by sortofsleepy on 9/1/2020.
//

#include "chibi/core/vk/DynamicUbo.h"

namespace chibi::vk {

    DynamicUbo::DynamicUbo(chibi::vk::VkRendererRef renderer) {}

    void DynamicUbo::setup(VkDeviceSize dataTypeSize) {
        auto swapchainSize = renderer->getSwapChain()->getSwapChainImageSize();

        for(int i = 0; i < swapchainSize; ++i){
            auto buff = Ubo::create(renderer);
            buff->setup(0);
            buff->init(dataTypeSize);

            uboBuffers.push_back(buff);

        }
    }
}