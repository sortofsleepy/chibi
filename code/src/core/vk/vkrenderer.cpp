//
// Created by sortofsleepy on 1/19/2020.
//

#include "chibi/core/vk/vkrenderer.h"

using namespace chibi;
using namespace std;

//TODO move this into an option
const int MAX_FRAMES_IN_FLIGHT = 3;

namespace chibi::vk{

    VkRenderer::~VkRenderer() {
        swapchain->teardown(logicalDevice);
        surface->teardown(instance);
    }

    void VkRenderer::setup(RendererFormat format){

        this->format = format;

        windowSurfaceChanged = false;

        // build up the application info
        VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "test";
        appInfo.applicationVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.pEngineName = "chibi-vulkan";
        appInfo.engineVersion = VK_MAKE_VERSION(1,0,0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        // setup necessary GLFW extensions
        uint32_t glfwExtensionCount = 0;
        const char ** glfwExtensions;
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        std::vector<const char * > extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

        for(int i = 0; i < extensions.size();++i){
            instanceExtensions.push_back(extensions[i]);
        }

        setupInstance(appInfo);
        setupPhysicalDevice();
        setupLogicalDevice();

        // setup graphics queue
        uint32_t graphicsFamily = physicalDeviceInfo.graphicsFamily;
        uint32_t presentFamily = physicalDeviceInfo.presentFamily;

        vkGetDeviceQueue(logicalDevice,graphicsFamily,0,&graphicsQueue);

        surface = vk::WindowSurface::create(instance,AppWindow::getInstance()->getGLFWWindow());

        // check for present support - continue only if available
        if(surface->getPresentSupport(&physicalDevice,presentFamily)){

            vkGetDeviceQueue(logicalDevice,presentFamily,0,&presentQueue);

            systemFoundationSetup = true;
            setupEngineFoundation();


        }else{
            throw std::runtime_error("Unable to continue - presentation support is not available for the current presentation family.");
        }
    }

    void VkRenderer::setupInstance(VkApplicationInfo appInfo) {
        // ================= BUILD INSTANCE =================== //
        VkInstanceCreateInfo instanceCreateInfo = {};
        instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCreateInfo.pApplicationInfo = &appInfo;


#ifdef DEBUG
        mValidator = chibi::vk::Validator::create();

        validationLayers.push_back(mValidator->getDebugValidationName());
        instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        instanceCreateInfo.ppEnabledLayerNames = validationLayers.data();
        instanceCreateInfo.pNext = mValidator->getCreateInfo();

#else
        instanceCreateInfo.enabledLayerCount = 0;
        instanceCreateInfo.pNext = nullptr;
#endif

        // build instance extensions
        if(!instanceExtensions.empty()){
            instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(instanceExtensions.size());
            instanceCreateInfo.ppEnabledExtensionNames = instanceExtensions.data();
        }else{
            instanceCreateInfo.enabledExtensionCount = 0;
            instanceCreateInfo.ppEnabledExtensionNames = nullptr;
        }

        VkResult result = vkCreateInstance(&instanceCreateInfo,nullptr,&instance);
        assert_vulkan(result);

#ifdef DEBUG
        mValidator->enableMessenger(&instance);
#endif
    }

    void VkRenderer::setupPhysicalDevice() {

        // enumerate all the physical devices
        uint32_t deviceCount = 0;
        vkEnumeratePhysicalDevices(instance,&deviceCount,nullptr);


        if(deviceCount == 0){
            throw std::runtime_error("Cannot continue - this system does not support Vulkan");
        }

        // retrieve list of physical devices
        physicalDevices.resize(deviceCount);
        vkEnumeratePhysicalDevices(instance,&deviceCount,physicalDevices.data());


        // gather up information about the physical devices available
        for (uint32_t i = 0; i < deviceCount; i++) {

            PhysicalDeviceInfo device;
            // get device properties.
            vkGetPhysicalDeviceProperties(physicalDevices[i], &device.deviceProperties);

            // get device features
            vkGetPhysicalDeviceFeatures(physicalDevices[i],&device.deviceFeatures);

            // get memory properties
            vkGetPhysicalDeviceMemoryProperties(physicalDevices[i],&device.deviceMemoryProperties);


            device.deviceIndex = i;
            availableDevices.push_back(device);

        }


        // now we loop through and see if the device matches our specifications, if it does then we set that as our physical device.
        for(int i = 0; i < availableDevices.size();++i){
            auto device = availableDevices[i];

            // if the physical device matches our Renderer settings, store the device's information
            if(format.physicalDeviceCheck(device)){

                // set the device if it has a dGPU - can pull from raw vulkan device list
                // since it should match the number of available devices.
                physicalDevice = physicalDevices[i];

                //! Set a reference to the object containing various aspects of the
                //! Physical device we want.
                physicalDeviceInfo = availableDevices[i];

                physicalDeviceInfo.device = physicalDevice;

#ifdef DEBUG
                LOG_I("Physical device selected was " << availableDevices[i].deviceProperties.deviceName);
#endif
            }
        }

        // ========================= BUILD QUEUE FAMILIES ============================== //
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice,&queueFamilyCount,nullptr);
        queueFamilies.resize(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice,&queueFamilyCount,queueFamilies.data());
        // loop through one more time and figure out graphics and presentaiton families of the device we select
        int i = 0;
        uint32_t graphicsFamily = 0;
        uint32_t presentFamily = 0;

        for(const auto &queueFamily : queueFamilies){

            if(queueFamily.queueCount > 0 && queueFamily.queueFlags && VK_QUEUE_GRAPHICS_BIT){
                graphicsFamily = i;
            }

            // present support is already stored

            if(queueFamily.queueCount > 0 && physicalDeviceInfo.presentSupport){
                presentFamily = i;
            }

            if(graphicsFamily != 0 && presentFamily != 0){
                break;
            }
        }


        physicalDeviceInfo.graphicsFamily = graphicsFamily;
        physicalDeviceInfo.presentFamily = presentFamily;

        // setup memory properties for the device.
        vkGetPhysicalDeviceMemoryProperties(physicalDevice,&physicalDeviceMemoryProperties);
    }

    uint32_t VkRenderer::getMemoryPropertyFlag(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
        VkPhysicalDeviceMemoryProperties memProperties = physicalDeviceMemoryProperties;
        //vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

        for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
            if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }
        }

        throw std::runtime_error("failed to find suitable memory type!");
    }

    //! Returns the queue family that supports VK_QUEUE_GRAPHICS BIT
    uint32_t getGraphicsBit(std::vector<VkQueueFamilyProperties> queueFamilyProperties){
        uint32_t bit = 0;
        for(uint32_t i = 0; i < queueFamilyProperties.size(); ++i){

            if(queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                bit = i;
                break;
            }
            i ++;
        }

        return bit;
    }

    void VkRenderer::setupLogicalDevice() {
        // specify queues to create
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = getGraphicsBit(queueFamilies);
        queueCreateInfo.queueCount = 1;

        float queuePriority = 1.0;
        queueCreateInfo.pQueuePriorities = &queuePriority;

        // start to set up the necessary information for the logical device.
        VkDeviceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.pQueueCreateInfos = &queueCreateInfo;
        createInfo.queueCreateInfoCount = 1;

        // TODO uncomment last line in this block of 3 lines
        VkPhysicalDeviceFeatures deviceFeatures = {};
        createInfo.pEnabledFeatures = &physicalDeviceInfo.deviceFeatures;

#ifdef DEBUG
        // setup validation layers
        std::vector<const char*> _layers = {
                "VK_LAYER_KHRONOS_validation"
        };
        std::vector<const char*> layers(_layers.begin(),_layers.end());

        createInfo.enabledLayerCount = static_cast<uint32_t>(layers.size());
        createInfo.ppEnabledLayerNames = layers.data();
#endif

        // TODO somehow inject custom selected logical extensions.
        std::vector<const char*> extensions = {
                VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };
        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        createInfo.ppEnabledExtensionNames = extensions.data();

        VkResult logicalCreation = vkCreateDevice(physicalDevice,&createInfo,nullptr,&logicalDevice);
        assert_vulkan(logicalCreation);
    }

    void VkRenderer::rebuildSwapchain() {

        windowSurfaceChanged = true;
        vkDeviceWaitIdle(logicalDevice);

        for (auto framebuffer : swapchainFramebuffers) {
            vkDestroyFramebuffer(logicalDevice, framebuffer, nullptr);
        }
        vkFreeCommandBuffers(logicalDevice, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

        vkDestroyRenderPass(logicalDevice, renderPass, nullptr);

        swapchain->teardown(logicalDevice);

        setupEngineFoundation();
    }

    void VkRenderer::setupEngineFoundation() {
        if(systemFoundationSetup || windowSurfaceChanged){

            setupSwapchain();
            setupRenderpass();
            setupFramebuffers();
            setupCommandBuffersAndPool();
            setupSemaphores();
            setupFences();

            // If the window surface changed, then reset the flag and run through any callbacks
            if(windowSurfaceChanged){

                for(auto &func : resizeCallbacks){
                    func();
                }

                windowSurfaceChanged = false;
            }
        }
    }

    void VkRenderer::setupSwapchain() {
        // init swap chain
        swapchain = SwapChain::create();

        // check for support, bail out if no swap chain capabilities are available.
        if(!swapchain->querySupport(&physicalDevice,surface)){
            throw std::runtime_error("Current device does not support a swap chain");
        }

        // set surface format and prepare extension
        swapchain->setSurfaceFormat();
        swapchain->setPresentMode();
        swapchain->compileSwapChain(logicalDevice,surface);

    }

    void VkRenderer::setupRenderpass() {
        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = swapchain->getImageFormat();

        // NOTE deals with multisampling
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

        // ========== LOAD OP / STORE OP =========
        // these determine what to do with the data in the attachment before rendering and after rendering.
        //  also applies to both color and depth. Stenciling has it's own load/store op as well.

        // VK_ATTACHMENT_LOAD_OP_LOAD - preserve the existing contents of the attachment
        // VK_ATTACHMENT_LOAD_OP_CLEAR - clear values to a constant at the start.
        // VK_ATTACHMENT_LOAD_OP_DONT_CARE - existing contents are undefined.
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;

        // VK_ATTACHMENT_STORE_OP_STORE - rendered contents will be stored in memory and can be read later.
        // VK_ATTACHMENT_STORE_OP_DONT_CARE - contents will be undefined after rendering.
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

        // ======================== LAYOUT ===================== //

        // Common layouts
        // VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL - images used as color attachment.
        // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR - Images to be presented in the swap chain
        // VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL - Images to be used as destination for copy.

        // specifies which layout the image will have before render pass begins.
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        // specifies the layout to  automatically transition to when the render pass finishes.
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


        // ================== ATTACHMENT REFERENCE ====================== //
        VkAttachmentReference colorAttachmentRef = {};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        //================ SUBPASSES ================ //
        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;

        // add subpass dependency to take care of transition at the start of render pass and at the end.
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


        // =========== RENDER PASS CREATION =============== //
        VkRenderPassCreateInfo renderpassInfo = {};
        renderpassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderpassInfo.attachmentCount = 1;
        renderpassInfo.pAttachments = &colorAttachment;
        renderpassInfo.subpassCount = 1;
        renderpassInfo.pSubpasses = &subpass;
        renderpassInfo.dependencyCount = 1;
        renderpassInfo.pDependencies = &dependency;


        VkResult res = vkCreateRenderPass(logicalDevice,&renderpassInfo,nullptr,&renderPass);
        if(res != VK_SUCCESS){
            throw std::runtime_error("Unable to create render pass");
        }
    }

    void VkRenderer::setupFramebuffers() {
        auto size = swapchain->getSwapChainImageViewsSize();

        swapchainFramebuffers.resize(size);

        for(int i = 0; i < size;++i){
            VkImageView attachments[] = {
                    swapchain->getImageViewAt(i)
            };

            VkFramebufferCreateInfo fb = {};
            fb.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            fb.renderPass = renderPass;
            fb.attachmentCount = 1;
            fb.pAttachments = attachments;
            fb.width = swapchain->getExtentWidth();
            fb.height = swapchain->getExtentHeight();

            fb.layers = 1;

            VkResult res = vkCreateFramebuffer(logicalDevice,&fb,nullptr,&swapchainFramebuffers[i]);
            if(res != VK_SUCCESS){
                throw std::runtime_error("Unable to create framebuffer");
            }
        }


    }

    void VkRenderer::setupFramebuffers(VkRenderPass rp) {
        auto size = swapchain->getSwapChainImageViewsSize();
        swapchainFramebuffers.resize(size);

        for(int i = 0; i < size;++i){
            VkImageView attachments[] = {
                    swapchain->getImageViewAt(i)
            };

            VkFramebufferCreateInfo fb = {};
            fb.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            fb.renderPass = rp;
            fb.attachmentCount = 1;
            fb.pAttachments = attachments;
            fb.width = swapchain->getExtentWidth();
            fb.height = swapchain->getExtentHeight();

            fb.layers = 1;

            VkResult res = vkCreateFramebuffer(logicalDevice,&fb,nullptr,&swapchainFramebuffers[i]);
            if(res != VK_SUCCESS){
                throw std::runtime_error("Unable to create framebuffer");
            }
        }
    }

    void VkRenderer::setupCommandBuffersAndPool() {

        // setup command pool
        VkCommandPoolCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.queueFamilyIndex = physicalDeviceInfo.graphicsFamily;

        //! Two possible flags
        // VK_COMMAND_POOL_CREATE_TRANSIENT_BIT - hit tha tcommand buffers are re-recorded with new commands very often
        // VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT - Allow command buffers to be re-recorded individually.
        info.flags = 0;

        VkResult res = vkCreateCommandPool(logicalDevice,&info,nullptr,&commandPool);
        if(res != VK_SUCCESS){
            throw std::runtime_error("Unable to create Command Pool");
        }

        // setup the command buffers
        commandBuffers.resize(swapchainFramebuffers.size());

        VkCommandBufferAllocateInfo bufferInfo = {};
        bufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        bufferInfo.commandPool = commandPool;

        // VK_COMMAND_BUFFER_LEVEL_PRIMARY - Can be subitted to queue, but cannot be called from other command buffers.
        // VK_COMMAN_BUFFER_LEVEL_SECONDARY - Cannot be submitted directly but can be called from primary command buffers.
        bufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

        bufferInfo.commandBufferCount = (uint32_t)commandBuffers.size();

        res = vkAllocateCommandBuffers(logicalDevice,&bufferInfo,commandBuffers.data());
        if(res != VK_SUCCESS){
            throw std::runtime_error("unable to create command buffers");
        }


    }

    void VkRenderer::setupSemaphores() {

        imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
        renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);

        VkSemaphoreCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i){

            if(vkCreateSemaphore(logicalDevice,&info,nullptr,&imageAvailableSemaphores[i]) ||
               vkCreateSemaphore(logicalDevice,&info,nullptr,&renderFinishedSemaphores[i])){
                throw std::runtime_error("Unable to create Semaphores");
            }
        }
    }

    void VkRenderer::setupFences(){
        inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
        VkFenceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i){
            if(vkCreateFence(logicalDevice,&createInfo,nullptr,&inFlightFences[i]) != VK_SUCCESS){
                throw std::runtime_error("Error in fence creation");
            }
        }


    }

    void VkRenderer::recordMultiple(chibi::vk::DrawCommand command) {
        for(int i = 0; i < commandBuffers.size(); ++i){
            VkCommandBufferBeginInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            info.pInheritanceInfo = nullptr;


            if(vkBeginCommandBuffer(commandBuffers[i],&info) != VK_SUCCESS){
                throw std::runtime_error("Unable to start command buffer recording");
            }

            // ============== START RENDER PASS ====================== //
            VkRenderPassBeginInfo rinfo = {};
            rinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            rinfo.renderPass = renderPass;
            rinfo.framebuffer = swapchainFramebuffers[i];
            rinfo.renderArea.offset = {0,0};
            rinfo.renderArea.extent = swapchain->getSwapChainExtent();


            VkDeviceSize offsets[] = {0};

            VkClearValue clearColor = {0.0f,0.0f,0.0f,1.0f};
            rinfo.clearValueCount = 1;
            rinfo.pClearValues = &clearColor;

            // kick off render pass.
            vkCmdBeginRenderPass(commandBuffers[i], &rinfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, command.pipeline);

            if(command.indexBuffer == VK_NULL_HANDLE){
                for(int a = 0; a < command.buffers.size();++a){
                    vkCmdBindVertexBuffers(commandBuffers[i],a,command.bindingCount,&command.buffers[a],&command.offsets[a]);
                }

                vkCmdDraw(commandBuffers[i], command.vertexCount, command.instanceCount, command.firstVertex, command.firstInstance);
            }else{
                for(int a = 0; a < command.buffers.size();++a){
                    vkCmdBindVertexBuffers(commandBuffers[i],a,command.bindingCount,&command.buffers[a],&command.offsets[a]);
                }

                vkCmdBindIndexBuffer(commandBuffers[i],command.indexBuffer,command.indexOffset,command.indexType);
                vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(command.numIndices),command.instanceCount,command.firstIndex,command.vertexOffset,command.firstInstance);

            }

            // end render pass
            vkCmdEndRenderPass(commandBuffers[i]);

            if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to record command buffer!");
            }
        }
    }
    void VkRenderer::record(chibi::vk::DrawCommand command) {
        for(int i = 0; i < commandBuffers.size(); ++i){
            VkCommandBufferBeginInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            info.pInheritanceInfo = nullptr;


            if(vkBeginCommandBuffer(commandBuffers[i],&info) != VK_SUCCESS){
                throw std::runtime_error("Unable to start command buffer recording");
            }


            // ============== START RENDER PASS ====================== //
            VkRenderPassBeginInfo rinfo = {};
            rinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            rinfo.renderPass = renderPass;
            rinfo.framebuffer = swapchainFramebuffers[i];
            rinfo.renderArea.offset = {0,0};
            rinfo.renderArea.extent = swapchain->getSwapChainExtent();


            VkDeviceSize offsets[] = {0};

            VkClearValue clearColor = {0.0f,0.0f,0.0f,1.0f};
            rinfo.clearValueCount = 1;
            rinfo.pClearValues = &clearColor;

            // kick off render pass.
            vkCmdBeginRenderPass(commandBuffers[i], &rinfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, command.pipeline);

            // TODO need to rethink this.
         //   vkCmdBindVertexBuffers(commandBuffers[i],command.firstBinding,command.bindingCount,command.buffers,offsets);

            vkCmdDraw(commandBuffers[i], command.vertexCount, command.instanceCount, command.firstVertex, command.firstInstance);

            // end render pass
            vkCmdEndRenderPass(commandBuffers[i]);

            if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to record command buffer!");
            }
        }
    }

    // TODO should probably expand to include support for inserting buffers, etc.
    void VkRenderer::record(VkPipeline graphicsPipeline) {
        for(int i = 0; i < commandBuffers.size(); ++i){
            VkCommandBufferBeginInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
            info.pInheritanceInfo = nullptr;


            if(vkBeginCommandBuffer(commandBuffers[i],&info) != VK_SUCCESS){
                throw std::runtime_error("Unable to start command buffer recording");
            }


            // ============== START RENDER PASS ====================== //
            VkRenderPassBeginInfo rinfo = {};
            rinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            rinfo.renderPass = renderPass;
            rinfo.framebuffer = swapchainFramebuffers[i];
            rinfo.renderArea.offset = {0,0};
            rinfo.renderArea.extent = swapchain->getSwapChainExtent();


            VkClearValue clearColor = {0.0f,0.0f,0.0f,1.0f};
            rinfo.clearValueCount = 1;
            rinfo.pClearValues = &clearColor;

            // kick off render pass.
            vkCmdBeginRenderPass(commandBuffers[i], &rinfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            // end render pass
            vkCmdEndRenderPass(commandBuffers[i]);

            if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to record command buffer!");
            }
        }
    }

    void VkRenderer::submitToGraphicsQueue(VkSubmitInfo info,uint32_t submitCount) {
        VkResult res = vkQueueSubmit(graphicsQueue, submitCount, &info,VK_NULL_HANDLE);

        vkQueueWaitIdle(graphicsQueue);
        if (res != VK_SUCCESS) {
            LOG_VULKAN(res);
            throw std::runtime_error("failed to submit draw command buffer!");
        }

    }
    void VkRenderer::drawFrame() {

        if(!systemFoundationSetup){
            return;
        }

        vkWaitForFences(logicalDevice,1,&inFlightFences[currentFrame],VK_TRUE,UINT64_MAX);
        vkResetFences(logicalDevice,1,&inFlightFences[currentFrame]);

        VkResult res = vkAcquireNextImageKHR(logicalDevice, swapchain->getSwapChain(), UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

        // if something has become out of date, rebuild all core parts of presenting code.
        if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR){
            rebuildSwapchain();
            return;
        }else if(res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR){
            throw std::runtime_error("vkAcquireNextImageKHR - unable to acquire swap chain image");
        }


        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = {imageAvailableSemaphores[currentFrame]};
        VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

        VkSemaphore signalSemaphores[] = {renderFinishedSemaphores[currentFrame]};
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        res = vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]);

        if (res != VK_SUCCESS) {
            LOG_VULKAN(res);
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;

        VkSwapchainKHR swapChains[] = {swapchain->getSwapChain()};
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;

        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr;

        vkQueuePresentKHR(presentQueue, &presentInfo);

        // wait for work to finish after submission
        vkQueueWaitIdle(presentQueue);

        currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;


    }
}// end namespace