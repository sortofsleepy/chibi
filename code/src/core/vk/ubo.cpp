//
// Created by sortofsleepy on 8/31/2020.
//

#include "chibi/core/vk/ubo.h"

namespace chibi::vk{

    Ubo::Ubo(chibi::vk::VkRendererRef renderer):renderer(renderer) {
        setup();
    }

    Ubo::~Ubo() {
        vkDestroyBuffer(*renderer->getLogicalDevice(),buffer,nullptr);
        vkFreeMemory(*renderer->getLogicalDevice(),bufferMemory,nullptr);
        vkDestroyDescriptorSetLayout(*renderer->getLogicalDevice(),layout,nullptr);
    }

    void Ubo::setup(uint32_t bindNumber) {
        VkDescriptorSetLayoutBinding layoutBinding{};
        layoutBinding.binding = bindNumber;
        layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        layoutBinding.descriptorCount = 1;
        layoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        layoutBinding.pImmutableSamplers = nullptr;

        VkDescriptorSetLayoutCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        createInfo.bindingCount = 1;
        createInfo.pBindings = &layoutBinding;

        VkResult res = vkCreateDescriptorSetLayout(*renderer->getLogicalDevice(),&createInfo,nullptr,&layout);
        if(res != VK_SUCCESS){
            throw std::runtime_error("Error creating UBO Descriptor layout");
        }

        usageFlags = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        memoryPropertyFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;



    }


    void Ubo::init(VkDeviceSize size) {
        bufferSize = size;
        auto device = *renderer->getLogicalDevice();

        VkBufferCreateInfo bufferInfo{};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size = bufferSize;
        bufferInfo.usage = usageFlags;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
            throw std::runtime_error("failed to create buffer!");
        }

        VkMemoryRequirements memRequirements;
        vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

        VkMemoryAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = memRequirements.size;
        allocInfo.memoryTypeIndex = renderer->getMemoryPropertyFlag(memRequirements.memoryTypeBits, memoryPropertyFlags);

        if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate buffer memory!");
        }

        vkBindBufferMemory(device, buffer, bufferMemory, 0);
    }
}

