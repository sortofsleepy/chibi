//
// Created by sortofsleepy on 1/18/2020.
//
#include "chibi/core/vk/swapchain.h"
#include <chibi/core/appwindow.h>
using namespace std;

namespace chibi::vk {

    void SwapChain::teardown(VkDevice device) {
        for (auto imageView : swapchainImageViews) {
            vkDestroyImageView(device, imageView->getImageView(), nullptr);
        }

        vkDestroySwapchainKHR(device,chain,nullptr);
    }

    void SwapChain::setPresentMode(VkPresentModeKHR mode) {
        bool found = false;
        for(const auto& modes : details.presentModes){
            if(modes == mode){
                presentMode = modes;
                found = true;
            }
        }
        if(!found){
            // FIFO_KHR is supposed to be always available.
            presentMode = VK_PRESENT_MODE_FIFO_KHR;
        }
    }
    bool SwapChain::querySupport(VkPhysicalDevice *device, chibi::vk::WindowSurfaceRef surface) {
        // get basic capabilities
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(*device,*surface->getVulkanSurface(),&details.capabilities);

        // get supported surface formats
        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(*device,*surface->getVulkanSurface(),&formatCount,nullptr);

        if(formatCount != 0){
            details.formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(*device,*surface->getVulkanSurface(),&formatCount,details.formats.data());
        }

        // get supported present formats.
        uint32_t presentcount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(*device,*surface->getVulkanSurface(),&presentcount,nullptr);


        if(presentcount != 0){
            details.presentModes.resize(presentcount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(*device,*surface->getVulkanSurface(),&presentcount,details.presentModes.data());
        }
        return !details.presentModes.empty() && !details.formats.empty();
    }

    void SwapChain::compileSwapChain(VkDevice logicalDevice, chibi::vk::WindowSurfaceRef surface,
                                     const chibi::vk::SwapchainOptions options) {
        uint32_t imageCount = details.capabilities.minImageCount;

        imageCount += 1;

        // setup the swapchain extent
        if(details.capabilities.currentExtent.width != UINT32_MAX){
            extent = details.capabilities.currentExtent;
        }else{
            auto win = AppWindow::getInstance();
            VkExtent2D actual = {static_cast<uint32_t>(win->getWidth()),static_cast<uint32_t>(win->getHeight())};

			actual.width = std::max(details.capabilities.minImageExtent.width,std::min(details.capabilities.maxImageExtent.width,actual.width));
            actual.height = std::max(details.capabilities.minImageExtent.height,std::min(details.capabilities.maxImageExtent.height,actual.height));

            extent = actual;
        }

        if(details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount){
            imageCount = details.capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = *surface->getVulkanSurface();
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = options.imageUsage;

        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

        createInfo.preTransform = details.capabilities.currentTransform;
        createInfo.compositeAlpha = options.alpha;


        createInfo.presentMode = presentMode;

        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;



        VkResult res = vkCreateSwapchainKHR(logicalDevice,&createInfo,nullptr,&chain);
        if(res != VK_SUCCESS){
            LOG_VULKAN(res);
            throw std::runtime_error("Unable to create swapchain");
        }

        vkGetSwapchainImagesKHR(logicalDevice,chain,&imageCount,nullptr);
        swapchainImages.resize(imageCount);


        vkGetSwapchainImagesKHR(logicalDevice,chain,&imageCount,swapchainImages.data());


        setImageViews(logicalDevice);
    }

    void SwapChain::setImageViews(VkDevice logicalDevice) {
        swapchainImageViews.resize(swapchainImages.size());
        for(int i = 0; i < swapchainImageViews.size(); ++i){
           ImageViewRef imageView = ImageView::create(swapchainImages[i],swapchainImageFormat);

           imageView->buildImageView(&logicalDevice);
           swapchainImageViews[i] = imageView;
        }
    }

    void SwapChain::setSurfaceFormat(VkFormat format, VkColorSpaceKHR colorspace) {
        bool found = false;
        for(const auto &fmt : details.formats){
            if(fmt.format == format && fmt.colorSpace == colorspace){
                found = true;
                surfaceFormat = fmt;
            }
        }

        if(!found){
            throw std::runtime_error("Error setting up Swap chain surface format - specified format and color space are not available on this device. ");
        }

        swapchainImageFormat = surfaceFormat.format;
    }
}
