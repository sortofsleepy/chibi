//
// Created by sortofsleepy on 12/26/2018.
//

#include <algorithm>
#include "chibi/core/vk/validator.h"
#include <iostream>
namespace chibi::vk {

    Validator::Validator() {
        createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = debugCallback;
        createInfo.pUserData = nullptr;
    }

        void Validator::setup(){}
        void Validator::teardown() {
            // destroy the messenger
            auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(*instance,"vkDestroyDebugUtilsMessengerEXT");
            if(func != nullptr){
                func(*instance,mMessenger,nullptr);
            }
        }

        VkResult Validator::enableMessenger(VkInstance * instance) {
            this->instance = instance;
            auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(*this->instance,"vkCreateDebugUtilsMessengerEXT");
            if(func != nullptr){
                return func(*instance,&createInfo,nullptr,&mMessenger);
            }else{
                return VK_ERROR_EXTENSION_NOT_PRESENT;
            }
        }


    }