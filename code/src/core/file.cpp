//
// Created by sortofsleepy on 2/11/2019.
//

#include "chibi/file.h"
using namespace std;
using namespace boost;
namespace fs = boost::filesystem;
namespace chibi::core {

    std::vector<char> File::loadSPV(const std::string &filename) {
        fs::path p(filename);

        // ensure we start at the application directory
        p = fs::current_path() / p;

        std::vector<char> buffer;

        // build stream for reading
        //fs::ifstream stream;

        if(fs::exists(p)){
            size_t fileSize = fs::file_size(p);
            buffer.resize(fileSize);
            std::ifstream stream(filename,std::ios::ate|std::ios::binary);

            stream.seekg(0);
            stream.read(buffer.data(),fileSize);
        }else{
            throw std::runtime_error("Unable to find file at " + filename);
        }


        return buffer;
    }

    std::string File::loadTextFile(std::string path) {

        fs::path p(path);

        // ensure we start at the application directory
        p = fs::current_path() / p;

        // build stream for reading
        fs::ifstream stream;

        // if path exists, read file, otherwise throw runtime error.
        if(fs::exists(p)){
            stream.open(p);
            std::string line;
            std::stringstream ss;
            if(stream.is_open()){
                ss << stream.rdbuf();
            }
            stream.close();
            return ss.str();
        }else {
            throw std::runtime_error("Unable to open file - " + path);
        }
    }
}
