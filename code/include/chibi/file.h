//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_FILE_H
#define CHIBI_FILE_H

#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"

namespace fs = boost::filesystem;
namespace chibi::core {

    /**
     * A basic wrapper around the concept of a file.
     */
    class File{

    public:
        //! loads a text file.
        static std::string loadTextFile(std::string path);


        //! Reads a SPIR-V file and returns it as a char vector
        static std::vector<char> loadSPV(const std::string &filename);

        //! Converts a string to a vector of chars. Useful for Vulkan stuff + shaders
        static std::vector<char> stringToChar(std::string str){
            return std::vector<char>(str.begin(),str.end());
        }

        //! Checks to see if the path is a directory
        static bool directoryExists(std::string path){
            bool val = false;
            if(fs::is_directory(path)){
                val = true;
            }
            return val;
        }

        //! Tests to see if the file exists.
        static bool fileExists(const std::string source){
            bool val = false;
            if(fs::exists(source)){
                val = true;
            }
            return val;
        }
    };
}

#endif //CHIBI_FILE_H
