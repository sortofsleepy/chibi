//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_MESH_H
#define CHIBI_MESH_H

#include <map>
#include <chibi/core/shader.h>
#include <utility>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <chibi/core/vk/swapchain.h>
#include <chibi/core/vk/vkrenderer.h>
#include "chibi/log.h"
#include "chibi/core/vk/buffer.h"
#include "chibi/core/vk/indexbuffer.h"
using namespace chibi::vk;

namespace chibi::framework{

    typedef std::shared_ptr<class Mesh> MeshRef;



    /**
     * A basic representation of a mesh.
     * Notes that it is assumed that mesh information is presented in
     * separate attributes for simpilicity sake.
     */
    class Mesh {
    public:
        Mesh() = default;
        Mesh(vk::VkRendererRef renderer,vk::ShaderRef shader):
        renderer(std::move(renderer)),
        shader(std::move(shader)){
            _defaultSettings();
        }

        static MeshRef create(vk::VkRendererRef renderer, vk::ShaderRef shader){
            return MeshRef(new Mesh(renderer,shader));
        }

        // adds information to the mesh.
        // You can optionally add a name in order to make it easier to look up later.
        template<typename T>
        Mesh& addData(std::vector<T> data, std::string name="");

        // adds an index buffer to the mesh.
        void addIndexBuffer(chibi::vk::IndexBufferRef indexBuffer){ this->indexBuffer = indexBuffer; }

        // adds indices to the mesh.
        template<typename A>
        void addIndices(std::vector<A> indices);

        // builds the renderass object
        void buildRenderPass(VkDevice device,SwapChainRef swapchain);

        // builds the graphics pipeline object
        void buildGraphicsPipeline(chibi::vk::SwapChainRef swapchain=nullptr);

        // returns the pipeline layout object
        VkPipelineLayout getPipelineLayout(){ return pipelineLayout;}

        // returns the pipeline object
        VkPipeline getPipeline(){ return graphicsPipeline;}

        // returns the renderpass object.
        VkRenderPass getRenderPass() { return renderPass; }

        // sets the number of instances for the mesh.
        void setNumInstances(int numInstances){ instanceCount = numInstances;}

        // draws the mesh
        void draw();

    protected:

        // holds the mesh's index buffer if necessary
        chibi::vk::IndexBufferRef indexBuffer;

        // stores settings used to render the mesh
        DrawCommand command;

        // a flag to help ensure the pipeline is built
        bool pipelineBuilt;

        // a flag to indicate whether or not the mesh viewport should constantly resize to fit window viewport.
        bool resizeToWindow;

        // reference to the main Vulkan renderer
        vk::VkRendererRef renderer;

        // reference to the shader for the mesh
        vk::ShaderRef shader;

        // sets up the default settings for the mesh
        void _defaultSettings();

        // a function to fire whenever the window size has changed.
        virtual void _rebuildPipeline();

        ////////// RANDOM SETTINGS ////////////
        VkBool32 primitiveRestart;
        int viewportCount;
        int scissorCount;

        ///////// VIEWPORT SETTINGS /////////////
        float viewWidth = 0.0f;
        float viewHeight = 0.0f;
        float viewX = 0.0f;
        float viewY = 0.0f;


        /////////// RENDERPASS SETTINGS ///////////////
        VkRenderPassCreateInfo renderPassInfo = {};
        VkSubpassDependency dependency = {};
        VkSubpassDescription subpass = {};
        VkAttachmentReference colorAttachmentRef = {};
        VkAttachmentDescription colorAttachment = {};

        /////////// PIPELINE SETTINGS /////////////////
        VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
        VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
        VkViewport viewport = {};
        VkRect2D scissor = {};
        VkPipelineViewportStateCreateInfo viewportState = {};
        VkPipelineRasterizationStateCreateInfo rasterizer = {};
        VkPipelineMultisampleStateCreateInfo multisampling = {};
        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        VkPipelineColorBlendStateCreateInfo colorBlending = {};
        VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
        VkGraphicsPipelineCreateInfo pipelineInfo = {};

        ///////////////////////////////////////////////

        // primitive type - ie points, triangles, etc
        VkPrimitiveTopology primitiveType;

        // pipeline layout object
        VkPipelineLayout  pipelineLayout;

        // pipeline object
        VkPipeline graphicsPipeline;

        // renderpass object.
        VkRenderPass renderPass;

        // a name that can be used when debugging.
        std::string debugName;

        // reference to vertex data
        std::vector<glm::vec4> vertices;

        // reference to UV data
        std::vector<glm::vec2> uvs;

        // reference to index data
        std::vector<uint32_t> indices;

        // vertices count
        uint32_t vertexCount;

        // number of instances
        uint32_t instanceCount;

        // a map of attributes for the mesh
        std::map<std::string,chibi::vk::BufferRef> attributes;

        // the final collection of buffers to send to the command buffers
        std::vector<VkBuffer> meshBuffers;

        // the final collection of offsets to send to the command buffers
        std::vector<VkDeviceSize> readOffsets;

        // stores attribute descriptions for the mesh
        std::vector<VkVertexInputAttributeDescription> attribDescriptions;

        // stores binding descriptions for the mesh.
        std::vector<VkVertexInputBindingDescription> bindingDescriptions;
    };
}
#endif //CHIBI_MESH_H
