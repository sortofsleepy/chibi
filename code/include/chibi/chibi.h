//
// Created by sortofsleepy on 12/15/2018.
//

#ifndef CHIBI_CHIBI_H
#define CHIBI_CHIBI_H




#ifdef GLFW_INCLUDE_VULKAN

#ifdef __APPLE__
    #include <MoltenVK/mvk_vulkan.h>
#else
   #include <vulkan/vulkan.h>
#endif

#elif ! defined(__EMSCRIPTEN__)
    #include <glad/glad.h>
#elif defined(__EMSCRIPTEN__)
    #include <emscripten/emscripten.h>
    #define GLFW_INCLUDE_ES3
    #include <GLES3/gl3.h>
#endif

#include "chibi/file.h"


#include <GLFW/glfw3.h>
#include <memory>

#endif //CHIBI_CHIBI_H
