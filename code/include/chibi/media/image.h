//
// Created by sortofsleepy on 12/16/2018.
//

#ifndef CHISAI_IMAGE_H
#define CHISAI_IMAGE_H

#ifdef __cplusplus

#include <memory>
#include <string>
#include "stbImage.h"

namespace chibi { namespace media {

        typedef std::shared_ptr<class Image>ImageRef;

        class Image {

            //! reference to raw image data.
            unsigned char * mData;

            //! width and height of an image.
            int width,height;
            int channels;
        public:
            explicit Image(std::string path);
            static ImageRef create(std::string path =""){
                return ImageRef(new Image(path));
            }

            //! load an image based off of raw data
            void loadImage(std::string path);


            int getWidth(){
                return width;
            }

            int getHeight(){
                return height;
            }

            unsigned char * getData(){
                return mData;
            }

        };

    }}

#endif

#endif //CHISAI_IMAGE_H

