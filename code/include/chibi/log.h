//
// Created by sortofsleepy on 12/13/2019.
//

#ifndef VULKAN_SIMPLE_LOG_H
#define VULKAN_SIMPLE_LOG_H
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "utils.h"
#include <vulkan/vulkan.h>

//! Gets the filename of the value returned from __FILE__ without
//! the full path.
// TODO currently outputs log.h as filename :/
static std::string getShortFilename(){
    std::string file = __FILE__;

    std::stringstream ss{file};

    std::string tok;
    std::vector<std::string> internal;

    while(std::getline(ss,tok,'/')){
        internal.push_back(tok);
    }

    return internal.back();
}

#define LOG_I( stream ) std::cout  << getShortFilename() <<" - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl
#define LOG_W( stream ) std::cout  << getShortFilename() <<" :WARNING - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl
#define LOG_E( stream ) std::cout  << getShortFilename() <<" :ERROR - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl


/**
 * Helper function to throw a runtime error anytime a VkResult is not true.
 * Will also print out helper message as to what the error could be.
 * TODO add other error codes.
 * @param result
 */
static bool assert_vulkan(VkResult result){

    if(result != VK_SUCCESS){
        switch(result){
            case VK_ERROR_DEVICE_LOST:
                LOG_E("Error - Vulkan device lost");
                break;

            case VK_ERROR_EXTENSION_NOT_PRESENT:
                LOG_E("Error - Extension is not present");
                break;

            case VK_ERROR_LAYER_NOT_PRESENT:
                LOG_E("Error - Requested layer is not present");
                break;

            case VK_ERROR_OUT_OF_HOST_MEMORY:
                LOG_E("Error - Out of Host Memory");
                break;

            case  VK_ERROR_MEMORY_MAP_FAILED:
                LOG_E("Error - Memory map failed");
                break;

            case VK_ERROR_VALIDATION_FAILED_EXT:
                LOG_E("Error - Validation failed");
                break;
        }

        throw std::runtime_error("");
    }

    return true;

}

static void getVulkunCode(VkResult res){
    switch(res){
        case VK_ERROR_DEVICE_LOST:
            LOG_E("Error - Vulkan device lost");
            break;

        case VK_ERROR_EXTENSION_NOT_PRESENT:
            LOG_E("Error - Extension is not present");
            break;

        case VK_ERROR_LAYER_NOT_PRESENT:
            LOG_E("Error - Requested layer is not present");
            break;

        case VK_ERROR_OUT_OF_HOST_MEMORY:
            LOG_E("Error - Out of Host Memory");
            break;

        case  VK_ERROR_MEMORY_MAP_FAILED:
            LOG_E("Error - Memory map failed");
            break;


    }
}
#define LOG_VULKAN( stream ) getVulkunCode(stream);

#endif //VULKAN_SIMPLE_LOG_H
