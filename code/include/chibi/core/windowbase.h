//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_WINDOW_H
#define CHIBI_WINDOW_H

#include "GLFW/glfw3.h"
#include <stdexcept>
#include <chibi/log.h>

namespace chibi::core {

        class Window {
        public:
            Window()= default;
            virtual void setup(int width,int height,const char* name){

                if(!glfwInit()){
                    throw std::runtime_error("Unable to initialize GLFW");
                }

                mWidth = width;
                mHeight = height;

#ifdef PLATFORM_VULKAN
                glfwWindowHint(GLFW_CLIENT_API,GLFW_NO_API);
#endif
                window = glfwCreateWindow(width,height,name,NULL,NULL);

                if(!window){
                    glfwTerminate();
                    LOG_E("Unable to initialize GLFW window");
                }

                glfwMakeContextCurrent(window);

                // set pointer to base window object so we can self-reference the class.
                glfwSetWindowUserPointer(window,this);

                auto resize = [](GLFWwindow * window, int width, int height)->void {
                    auto win = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
                    win->mWidth = width;
                    win->mHeight = height;
                };


                glfwSetFramebufferSizeCallback(window,resize);
            }

            static void framebufferResizeCallback(GLFWwindow * window, int width, int height){

            }
#ifdef PLATFORM_VULKAN
            void setGLFWInstanceExtensions( std::vector<const char*> &instanceExtensions) {
                uint32_t glfwExtensionCount = 0;
                const char ** glfwExtensions;
                glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

                std::vector<const char * > extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

                for(auto extension : extensions){
                    instanceExtensions.push_back(extension);
                }
            }

#endif

            void resize(GLFWwindow* window, int width, int height){

            }

            void stopGLFW(){
                glfwTerminate();
            }

            GLFWwindow * getGLFWWindow(){
                return window;
            }

            float getAspect(){
                return mWidth / mHeight;
            }

            int getWidth(){
                return mWidth;
            }

            int getHeight(){
                return mHeight;
            }

            void setWidth(int width){
                mWidth = width;
            }
            void setHeight(int height){
                mHeight = height;
            }
        protected:
            GLFWwindow * window;
            int mWidth = 0;
            int mHeight = 0;
        };
    }

#endif //CHIBI_WINDOW_H
