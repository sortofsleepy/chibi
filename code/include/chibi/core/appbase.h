//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_APPBASE_H
#define CHIBI_APPBASE_H

#include <utility>

#include "chibi/core/appwindow.h"


namespace chibi::core {

    class AppBase {
    public:
        AppBase():
        appName(std::move(appName)),
        appTime(0.0f){

        }

        ~AppBase(){
            glfwTerminate();
        }
        //! Returns the current app runtime
        float getTime(){ return appTime; }

        //! Setup function
        virtual void setup(){}

        //! Draw function
        virtual void draw(){}

        //! Kicks off render loop.
        void run(){
            auto window = AppWindow::getInstance();

            while(!glfwWindowShouldClose(window->getGLFWWindow())){
                updateAppTime();


                glfwSwapBuffers(window->getGLFWWindow());
                draw();

                glfwPollEvents();
            }
        }

    protected:


        //! Updates the app runtime.
        void updateAppTime(){
            auto time = static_cast<float>(glfwGetTime());
            appTime = time * 1000.0;
        }

        //! Refers to the name for the app.
        std::string appName;

        //! Refers to the current runtime for the app.
        float appTime;
    };

}


#endif //CHIBI_APPBASE_H
