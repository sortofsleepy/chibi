//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_SHADER_H
#define CHIBI_SHADER_H

#include <string>
#include <utility>
#include "chibi/chibi.h"
#include "chibi/log.h"
#include "chibi/file.h"
#ifdef PLATFORM_VULKAN
#include "shaderc/shaderc.hpp"
#endif

namespace chibi::vk {


    /**
     * Defines a Vulkan Shader. Stores all possible stages as well as default vertex + fragment stages.
     */
    typedef std::shared_ptr<class Shader>ShaderRef;
    class Shader {

    public:

        /**
         * Describes the basic necessities of a Shader.
         */
        struct ShaderFormat {

            ShaderFormat() = default;

            std::string _vertex = "";
            std::string _fragment = "";
            std::string _geometry = "";
            std::string _compute = "";

            //! Set the content for the vertex shader. Can be raw shader source or path to source file.
            ShaderFormat& vertex(std::string src){
                _vertex = src;
                return *this;
            }

            //! Set the content for the fragment shader. Can be raw shader source or path to source file.
            ShaderFormat& fragment(std::string src){
                _fragment = src;
                return *this;
            }

            //! checks to see if the vertex shader has been set with a source or path.
            bool vertexSet() {
                return !_vertex.empty();
            }

            //! checks to see if the fragment shader has been set with a source or path.
            bool fragmentSet() {
                return !_fragment.empty();
            }

        };



        explicit Shader(ShaderFormat format);
        explicit Shader(ShaderFormat format,VkDevice * device);

        static ShaderRef create(const ShaderFormat format) {
            return ShaderRef(new Shader(format));
        }
        static ShaderRef create(const ShaderFormat format, VkDevice *pT) {
            return ShaderRef(new Shader(format,pT));
        }

        //! Generate shader modules.
        void generateShaderModules(VkDevice * device);

        //! Tears down shader modules
        void teardownModules(VkDevice * device) {
            vkDestroyShaderModule(*device,vertexModule,nullptr);
            vkDestroyShaderModule(*device,fragmentModule, nullptr);
        }

        // returns information on shader sources.
        std::vector<VkPipelineShaderStageCreateInfo> *getShaderInfo(){ return &shaderInfo;}
    protected:

        //! Holds reference information to shader sources
        std::vector<VkPipelineShaderStageCreateInfo> shaderInfo;

        //! compiles shader source into
        void compile(VkDevice * device,std::string source_name, shaderc_shader_kind kind,std::string source, bool optimize=true);

        //! Checks to see if the string could be shader source.
        bool isShaderSource(std::string srcOrPath);

        //! Process vertex shader
        void buildVertexShader(VkDevice *device);

        //! Process fragment shader
        void buildFragmentShader(VkDevice *device);

        //! Shader settings
        //! TODO make reference.
        ShaderFormat format;

        //! Modules for shader source.
        VkShaderModule vertexModule,fragmentModule,geometryModule,computeModule;

    };

}

#endif //CHIBI_SHADER_H
