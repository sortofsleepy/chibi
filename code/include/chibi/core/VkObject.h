//
// Created by sortofsleepy on 7/20/2020.
//

#ifndef CHIBI_VKOBJECT_H
#define CHIBI_VKOBJECT_H

/**
 * VkObject
 * Helps enforce certain conventions around what functions a Vulkan related
 * object should have.
 */
class VkObject {
public:
    VkObject() = default;

    // called when there is a need to rebuild / close out an object
    virtual void teardown() = 0;

    // called when something about the current swapchain image becomes dirty, ie window resizing.
    virtual void onDirty() = 0;

    // a name that can be used to refer to the object for debugging purposes.
    std::string name = "";

    void log(std::string message){
        LOG_I("Object[" << name  << "]" << message);
    }
};

#endif //CHIBI_VKOBJECT_H
