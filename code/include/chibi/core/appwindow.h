//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_VKWINDOW_H
#define CHIBI_VKWINDOW_H

#include "chibi/core/windowbase.h"
#include "chibi/chibi.h"


namespace chibi  {

    typedef std::shared_ptr<class AppWindow> WindowRef;
    static WindowRef instance = nullptr;

    class AppWindow : public chibi::core::Window{
    public:
        static WindowRef getInstance()
        {
            if(instance == nullptr){
                instance = AppWindow::create();
            }

            // Instantiated on first use.
            return instance;
        }

        static WindowRef create(){
            return WindowRef(new AppWindow);
        }

    protected:
        AppWindow(){}
    };


}
#endif //CHIBI_VKWINDOW_H
