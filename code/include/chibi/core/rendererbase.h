//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_RENDERERBASE_H
#define CHIBI_RENDERERBASE_H


#include "chibi/chibi.h"
#include <functional>
namespace chibi::core {

    class RenderBase {

    public:
        RenderBase() = default;

        // some Vulkan Specific stuff to include.

        //! describes various properties of available physical devices installed in the system
        typedef struct {
            //! Holds information about the physical device
            VkPhysicalDeviceProperties deviceProperties;

            //! Holds information about device features.
            VkPhysicalDeviceFeatures deviceFeatures;

            //! Holds information about deivce memory
            VkPhysicalDeviceMemoryProperties deviceMemoryProperties;

            //! Whether or not there is presentation support on the VkPhysicalDevice
            VkBool32  presentSupport;

            //! Holds the current index of the present family.
            uint32_t presentFamily;

            //! Holds the current index of the current graphics family.
            uint32_t graphicsFamily;

            //! A reference to the actual Physical Device.
            VkPhysicalDevice device;

            //! Device index
            uint32_t deviceIndex;
        }PhysicalDeviceInfo;

        //! describes various settings we would like in place for the renderer.
        struct RendererFormat {
            std::string appName = "chibi-app";
            uint32_t apiVersion =  VK_API_VERSION_1_0;
            uint32_t engineVersion = VK_MAKE_VERSION(1,0,0);
            uint32_t applicationversion = VK_MAKE_VERSION(1,0,0);

            // how many frames to have available at any given time for renderering / presentation.
            uint32_t framesInFlight = 3;

            //! Describes the function used to check if the selected physical device is acceptable for your rendering needs.
            //! By default it just checks to see if there's a discrete GPU.
            std::function<bool(PhysicalDeviceInfo)> physicalDeviceCheck = [=](PhysicalDeviceInfo device)->bool{

                // we want one that has is a dGPU as well as support a geometry shader.
                // TODO maybe make list of desired features customizable. Right now just take the last one that matches.
                if(device.deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU &&
                   device.deviceFeatures.geometryShader){
                    return true;
                }

                return false;
            };
        };


    };
}

#endif //CHIBI_RENDERERBASE_H
