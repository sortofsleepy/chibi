//
// Created by sortofsleepy on 11/19/2018.
//

#ifndef CHISAI_VALIDATOR_H
#define CHISAI_VALIDATOR_H

#include <vector>
#include <string>
#include <cstring>
#include <memory>
#include <stdexcept>
#include "chibi/chibi.h"
#include "chibi/log.h"


namespace chibi { namespace vk {

        typedef std::shared_ptr<class Validator> ValidatorRef;

        class Validator {
        public:
            Validator();

            static ValidatorRef create(){
                auto validator = new Validator();
                return ValidatorRef(validator);
            }

            const char * getDebugValidationName(){
                // appears to not exist anymore
                //return "VK_LAYER_KHRONOS_validation";
                return "VK_LAYER_LUNARG_standard_validation";
            }

            const char * getLogicalDeviceValidationLayerName(){
                return "VK_LAYER_KHRONOS_validation";
            }

            void teardown();
            VkResult enableMessenger(VkInstance * instance);

            //! run through any necessary setup to enable validation layers.
            void setup();

            VkDebugUtilsMessengerCreateInfoEXT* getCreateInfo(){ return &createInfo;}

            static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
                    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                    VkDebugUtilsMessageTypeFlagsEXT messageType,
                    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                    void* pUserData
            ){

// TODO make verbosity optional.
                switch (messageSeverity){
                case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:

                    LOG_E("[ERROR] Validation layer message : " << pCallbackData->pMessage);
                    break;

                case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                    LOG_I("[INFO] Validation layer message: " << pCallbackData->pMessage);
                    break;

                case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                    LOG_W("[WARNING] Validation layer message : " << pCallbackData->pMessage);
                    break;

                case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                    LOG_I("Verbose validation layer message : " << pCallbackData->pMessage);
                    break;

            }


                return VK_FALSE;
            }
        private:

            std::vector<const char*> mLayers;
            VkDebugUtilsMessengerEXT  mMessenger;
            VkInstance * instance;
            VkDebugUtilsMessengerCreateInfoEXT createInfo;
        };


    }} // end chibi::vk
#endif //CHISAI_VALIDATOR_H
