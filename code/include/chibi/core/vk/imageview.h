//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_IMAGEVIEW_H
#define CHIBI_IMAGEVIEW_H

#include "chibi/chibi.h"
#include "chibi/log.h"
namespace chibi::vk{
    typedef std::shared_ptr<class ImageView> ImageViewRef;


    typedef struct {

        VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D;
        VkComponentSwizzle red = VK_COMPONENT_SWIZZLE_IDENTITY;
        VkComponentSwizzle green = VK_COMPONENT_SWIZZLE_IDENTITY;
        VkComponentSwizzle blue = VK_COMPONENT_SWIZZLE_IDENTITY;
        VkComponentSwizzle alpha = VK_COMPONENT_SWIZZLE_IDENTITY;

        VkImageAspectFlags aspectFlag = VK_IMAGE_ASPECT_COLOR_BIT;
        uint32_t baseMipLevel = 0;
        uint32_t levelCount = 1;
        uint32_t baseArraylayer = 0;
        uint32_t layerCount = 1;
    }ImageViewOptions;
/**
 * A wrapper around VkImageView. An image view describes how to access the image and which part of
 * the image to access.
 */
    class ImageView {
    public:
        ImageView(VkImage image,VkFormat imageFormat, ImageViewOptions options):
                createInfo({}),
                imageView(VK_NULL_HANDLE){

            createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            createInfo.viewType = options.viewType;
            createInfo.format = imageFormat;
            createInfo.image = image;
            createInfo.components.r = options.red;
            createInfo.components.g = options.green;
            createInfo.components.b = options.blue;
            createInfo.components.a = options.alpha;

            createInfo.subresourceRange.aspectMask = options.aspectFlag;
            createInfo.subresourceRange.baseMipLevel = options.baseMipLevel;
            createInfo.subresourceRange.levelCount = options.levelCount;
            createInfo.subresourceRange.baseArrayLayer = options.baseArraylayer;
            createInfo.subresourceRange.layerCount = options.layerCount;
        }

        //! Default destructor, due to needing to pass in a Vulkan Logical device.
        ~ImageView() = default;

        void destroy(VkDevice device){
            vkDestroyImageView(device,imageView,nullptr);
        }

        static ImageViewRef create(VkImage image, VkFormat imageFormat, ImageViewOptions options = ImageViewOptions()){
            return ImageViewRef(new ImageView(image,imageFormat,options));
        }

        //! Builds the image view.
        void buildImageView(VkDevice * device){

            assert_vulkan(vkCreateImageView(*device,&createInfo,nullptr,&imageView));
        }

        VkImageView getImageView(){ return imageView; }
    protected:
        VkImageView imageView;
        VkImageViewCreateInfo createInfo;
    };
}

#endif //CHIBI_IMAGEVIEW_H
