//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_SURFACE_H
#define CHIBI_SURFACE_H

#include "chibi/chibi.h"
#include "chibi/log.h"

namespace chibi::vk {

    typedef std::shared_ptr<class WindowSurface> WindowSurfaceRef;

/**
 * A wrapper around necessary work to establish a Window surface.
 * Currently only supports Windows OS
 */
    class WindowSurface {
    public:
        WindowSurface(VkInstance instance, GLFWwindow *window):
                surface(VK_NULL_HANDLE),
                presentQueue(VK_NULL_HANDLE) {

            assert_vulkan(glfwCreateWindowSurface(instance,window, nullptr,&surface));
        }

        void teardown(VkInstance instance){
            vkDestroySurfaceKHR(instance,surface,nullptr);
        }

        static WindowSurfaceRef create(VkInstance instance, GLFWwindow * window){
            return WindowSurfaceRef(new WindowSurface(instance,window));
        }



        // checks for presentation support based on the current physical device and selected queue index.
        bool getPresentSupport(VkPhysicalDevice * device,uint32_t queueIndex){
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(*device,queueIndex,surface,&presentSupport);

            return presentSupport != 0;

        }
        VkSurfaceKHR* getVulkanSurface() { return &surface; }
    protected:
        VkSurfaceKHR surface;

        //! The presentation queue for this surface.
        VkQueue presentQueue;

    };
}
#endif //CHIBI_SURFACE_H
