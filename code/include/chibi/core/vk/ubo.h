//
// Created by sortofsleepy on 8/31/2020.
//

#ifndef CHIBI_UBO_H
#define CHIBI_UBO_H


#include "chibi/chibi.h"
#include "chibi/core/vk/buffer.h"


namespace chibi::vk {

    typedef std::shared_ptr<class Ubo> UboRef;
    class Ubo {

    public:

        Ubo(chibi::vk::VkRendererRef renderer);
        ~Ubo();

        static UboRef create(chibi::vk::VkRendererRef renderer){
            return UboRef(new Ubo(renderer));
        }

        void setup(uint32_t bindNumber=0);

        // initializes the UBO
        void init(VkDeviceSize size);
    protected:
        chibi::vk::VkRendererRef renderer;
        VkDescriptorSetLayout layout;

        // size of the datatype the UBO is going to contain.
        VkDeviceSize bufferSize;

        // buffer object
        VkBuffer buffer;

        // memory for the buffer
        VkDeviceMemory bufferMemory;

        // usage flags for the buffer
        VkBufferUsageFlags usageFlags;

        // memory prop flags for the buffer.
        VkMemoryPropertyFlags memoryPropertyFlags;

    };

}

#endif //CHIBI_UBO_H
