//
// Created by sortofsleepy on 1/18/2020.
//

#ifndef CHIBI_SWAPCHAIN_H
#define CHIBI_SWAPCHAIN_H

#include "chibi/chibi.h"
#include "surface.h"
#include <vector>
#include <algorithm>
#include "chibi/core/vk/imageview.h"

namespace chibi::vk {
    typedef struct {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    }SwapChainSupportDetails;

    // an options object to help with setting up a Swapchain
    typedef struct {
        VkSharingMode sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        std::vector<uint32_t> queueFamilies;
        VkSurfaceTransformFlagBitsKHR transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        VkCompositeAlphaFlagBitsKHR alpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        VkBool32 clipped = VK_TRUE;
        VkSwapchainKHR  oldSwapchain = VK_NULL_HANDLE;
        VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }SwapchainOptions;

    typedef std::shared_ptr<class SwapChain> SwapChainRef;

    //! A basic wrapper around a Vulkan SwapChain, which is a list of image buffers that are eventually displayed to the user
    class SwapChain {
    public:
        SwapChain() = default;

        void teardown(VkDevice device);

        //! returns the necessary name to create a swapchain
        //! TODO not sure if there are other names we need to account for.
        const char* getExtentionName(){ return VK_KHR_SWAPCHAIN_EXTENSION_NAME; }

        static SwapChainRef create(){
            return SwapChainRef(new SwapChain);
        }

        //! Queries for swap chain support
        bool querySupport(VkPhysicalDevice * device, WindowSurfaceRef surface);

        //! Sets the surface format for the swap chain.
        void setSurfaceFormat(VkFormat format=VK_FORMAT_B8G8R8A8_UNORM, VkColorSpaceKHR colorspace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);

        VkFormat getImageFormat(){ return swapchainImageFormat; }

        //! Compiles and builds the swapchain elements
        void compileSwapChain(VkDevice logicalDevice, WindowSurfaceRef surface, const SwapchainOptions options=SwapchainOptions());

        //! Set presentation mode for the swap chain
        void setPresentMode(VkPresentModeKHR mode = VK_PRESENT_MODE_MAILBOX_KHR);

        VkExtent2D getSwapChainExtent(){ return extent; }

        int getSwapChainImageViewsSize(){ return swapchainImageViews.size(); };

    	std::vector<ImageViewRef> getImageViews(){ return swapchainImageViews;}
        VkImageView getImageViewAt(int index){return swapchainImageViews.at(index)->getImageView(); }

        uint32_t getExtentWidth(){ return extent.width; }
        uint32_t getExtentHeight() { return extent.height; }

        uint32_t getSwapChainImageSize(){ return swapchainImages.size(); }

        void setImageViews(VkDevice logicalDevice);
        VkSwapchainKHR getSwapChain(){return chain;}
    protected:
        //! Holds image views for the swap chain
        std::vector<ImageViewRef> swapchainImageViews;

        //! Holds images for the swap chain.
        std::vector<VkImage> swapchainImages;

        //! The actual swapchain object
        VkSwapchainKHR chain;

        //! Information about the swap chain.
        SwapChainSupportDetails details;

        //! The bounds of the app window
        VkExtent2D extent;

        //! Specifies the color settings for the swap chain images.
        VkSurfaceFormatKHR surfaceFormat;

        //! Holds the format to use for ech of the swapchain images
        VkFormat swapchainImageFormat;

        //! Represents the conditions for showing an image to the screen
        VkPresentModeKHR presentMode;

        VkExtent2D swapChainExtent;

    };
}
#endif //CHIBI_SWAPCHAIN_H
