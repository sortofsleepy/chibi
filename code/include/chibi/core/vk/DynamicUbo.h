//
// Created by sortofsleepy on 8/31/2020.
//

#ifndef CHIBI_DYNAMICUBO_H
#define CHIBI_DYNAMICUBO_H

#include "chibi/chibi.h"
#include "chibi/core/vk/ubo.h"

namespace chibi::vk {

    std::shared_ptr<class DynamicUbo>DynamicUboRef;

    /**
     * Works like a regular Ubo but is intended for dynamic content
     * and will create and operate the necessary number of buffers to
     * ensure updatability.
     */
    class DynamicUbo {

    public:
        DynamicUbo(VkRendererRef renderer);

        static DynamicUboRef create(VkRendererRef renderer){
            return DynamicUboRef(new DynamicUbo(renderer));
        }


        void setup(VkDeviceSize dataTypeSize);

    protected:
        chibi::vk::VkRendererRef renderer;

        // ubo buffers. We need multiple buffers due to needing to keep things
        // updated
        std::vector<chibi::vk::UboRef> uboBuffers;
        //std::vector<VkDeviceMemory> uboMemory;
    };

}// end namespace

#endif //CHIBI_DYNAMICUBO_H
