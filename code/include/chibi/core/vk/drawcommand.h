//
// Created by sortofsleepy on 7/20/2020.
//

#ifndef CHIBI_DRAWCOMMAND_H
#define CHIBI_DRAWCOMMAND_H

#include "chibi/chibi.h"

/**
 * This describes all of the possible settings to run when drawing something to
 * screen.
 */
namespace chibi::vk {
    typedef struct {

        VkPipeline pipeline;

    // ======== PRIMARILY ARRAWY DAA
        uint32_t firstBinding = 0;
        uint32_t bindingCount = 1;
        uint32_t vertexCount = 1;
        uint32_t instanceCount = 1;
        uint32_t  firstVertex = 0;
        uint32_t firstInstance = 0;

        std::vector<VkBuffer> buffers;


        VkBuffer indexBuffer = VK_NULL_HANDLE;
        VkIndexType indexType = VK_INDEX_TYPE_UINT16;
        uint32_t indexOffset = 0;
        uint32_t numIndices = 0;
        uint32_t firstIndex = 0;
        uint32_t vertexOffset = 0;

        std::vector<VkDeviceSize> offsets;

        void logData(int numBuffers, int numOffsets){


            LOG_I("firstBinding: " << firstBinding);
            LOG_I("bindingCount: " << bindingCount);
            LOG_I("num buffers: " << numBuffers);
            LOG_I("num offsets: " << numOffsets);
            LOG_I("vertex count: " << vertexCount);
            LOG_I("instance count: " << instanceCount);
            LOG_I("first vertex: " << firstVertex);
            LOG_I("first Instance: " << firstInstance);

            LOG_I("\n");


        }
    }DrawCommand;
}

#endif //CHIBI_DRAWCOMMAND_H
