//
// Created by sortofsleepy on 1/19/2020.
//

#ifndef CHIBI_VKRENDERER_H
#define CHIBI_VKRENDERER_H

#include "chibi/chibi.h"
#include "chibi/core/vk/validator.h"
#include "chibi/core/appwindow.h"
#include "chibi/core/rendererbase.h"
#include "chibi/core/vk/surface.h"
#include "chibi/core/vk/swapchain.h"
#include "chibi/core/vk/drawcommand.h"
namespace chibi::vk{

    typedef std::shared_ptr<class VkRenderer>VkRendererRef;



    /**
     * This is the renderer class. It's purpose is to handle all of the
     * insane amount of setup work needed to bring something to the screen.
     *
     * Currently it manages the physical and logical devices needed as well as necessary sub-objects.
     *
     */
    class VkRenderer : public core::RenderBase {
    public:
        VkRenderer() = default;

        ~VkRenderer();

        static VkRendererRef create(){
            return VkRendererRef(new VkRenderer);
        }

        //! Start the general setup of the render foundation
        void setup(RendererFormat format = RendererFormat());

        //! Sets up the Vulkan Instance
        void setupInstance(VkApplicationInfo appInfo);

        //! Sets up the physical device
        void setupPhysicalDevice();

        //! Sets up the logical device.
        void setupLogicalDevice();

        //! Begins to set up the rest of the necessities needed to render things.
        //! This is done separately after we verify that we have presentation support
        void setupEngineFoundation();

        //! Setup render passes
        void setupRenderpass();

        //! Sets up framebuffers - allows for use of custom renderpass.
        void setupFramebuffers(VkRenderPass rp);

        //! Sets up the swapchain
        void setupSwapchain();

        void rebuildSwapchain();

        //! Sets up framebuffers
        void setupFramebuffers();

        //! Sets up the command pool and command buffers
        void setupCommandBuffersAndPool();

        //! Sets up semaphores
        void setupSemaphores();

        //! Sets up wait fences
        void setupFences();

        uint32_t getMemoryPropertyFlag(uint32_t typeFilter, VkMemoryPropertyFlags properties);

        // ========== GETTERS ========== //

        //! Returns a reference to the logical device
        VkDevice * getLogicalDevice(){ return &logicalDevice; }

        //! Returns a reference to the swap chain object
        SwapChainRef getSwapChain(){ return swapchain; }

        // return the physical device.
        VkPhysicalDevice * getPhysicalDevicee() { return &physicalDevice; }

        // returns the current command pool
        VkCommandPool * getRendererCommandPool(){ return &commandPool; }

        // returns the current graphics queue
        VkQueue* getGraphicsQueue(){return &graphicsQueue; }

        // returns the current presentation queue
        VkQueue* getPresentQueue(){return &presentQueue; }


        // ======= DRAWING / RUNNING =============== //

        //! Records a new graphics pipeline onto the command buffers
        void record(VkPipeline graphicsPipeline);

        //! Records a mesh into the command buffers based on a single VkBuffer
        void record(DrawCommand command);

        //! Records a mesh into the command buffers based on multiple VkBuffers
        void recordMultiple(DrawCommand command);

        // Presents information onto the graphics queue.
        void submitToGraphicsQueue(VkSubmitInfo info,uint32_t submitCount=1);

        //! Renders a frame.
        void drawFrame();


        //! Returns whether or not the window surface changed.
        bool hasWindowSurfaceChanged(){ return windowSurfaceChanged; };

        void addResizeHandler(std::function<void()> func) {
            resizeCallbacks.push_back(func);
        }
    private:

        std::vector<std::function<void()> > resizeCallbacks;

        // ================ FOUNDATION ======================= //

        // a flag to mark whenever the window surface has changed.
        bool windowSurfaceChanged;

        //! A flag to check for when the foundational components of the engine are set up
        bool systemFoundationSetup = false;

        //! Holds the settings for the renderer
        RendererFormat format;

        //! Manages how vulkan interacts with the GLFW window
        vk::WindowSurfaceRef surface;

        //! Queues to manage rendering + presentation
        VkQueue graphicsQueue,presentQueue;

        // information on the currently selected device.
        PhysicalDeviceInfo physicalDeviceInfo;

        // reference to the validation object.
        ValidatorRef mValidator;

        // Vulkan instance; the connection between application and Vulkan library
        VkInstance instance;

        //! Reference to the GPU - we just take the first device.
        // TODO in the future, maybe device selection could be passed in as a param.
        VkPhysicalDevice physicalDevice;

        //! A list of available physical devices.
        std::vector<VkPhysicalDevice> physicalDevices;

        //! Holds a list of the properties of any available devices. Should be the same size as the devices vector.
        std::vector<PhysicalDeviceInfo> availableDevices;

        //! Makes a list of various properties in the available queue families.
        std::vector<VkQueueFamilyProperties> queueFamilies;

        //! Logical device helps to describe the features that we want to use.
        VkDevice logicalDevice;

        // all of the validation layers that need to be supported and run
        std::vector<const char*> validationLayers;

        // all of the extensions that need to be supported in order to run.
        std::vector<const char*> deviceExtensions;

        //! All of the instance extensions that needs to be supported
        std::vector<const char*> instanceExtensions;

        // ================ ENGINE FOUNDATION ======================= //

        // holds memory capabilities of the current physical device.
        VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;

        //! An object to help manage the swap chain
        vk::SwapChainRef swapchain;

        //! A default render pass
        //! TODO eventually make it so we always pass in a render pass
        VkRenderPass  renderPass;

        //! Hold framebuffers used to render things
        std::vector<VkFramebuffer> swapchainFramebuffers;

        //! Command pool for helping to manage how drawing is dispatched
        VkCommandPool commandPool;

        //! Buffers to record commands onto
        std::vector<VkCommandBuffer> commandBuffers;

        //! Semaphores - used to insert a dependency between queue operations or between a queue operation and the host.
        std::vector<VkSemaphore> imageAvailableSemaphores,renderFinishedSemaphores;

        // fences are used to sync between cpu / gpu
        std::vector<VkFence> inFlightFences;

        // the current frame rendering
        size_t currentFrame = 0;

        //! Current Swapchain image being used.
        uint32_t imageIndex = 0;
    };

}// end namespace
#endif //CHIBI_VKRENDERER_H
