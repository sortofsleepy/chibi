//
// Created by sortofsleepy on 7/16/2020.
//

#ifndef CHIBI_BUFFER_H
#define CHIBI_BUFFER_H
#include "chibi/chibi.h"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/vec2.hpp"
#include "chibi/core/vk/vkrenderer.h"
#include "chibi/utils.h"
namespace chibi::vk {

    typedef std::shared_ptr<class Buffer> BufferRef;
    class Buffer {
    public:
        Buffer();

        static BufferRef create(){
            return BufferRef(new Buffer);
        }

        VkBuffer getBuffer(){ return buffer; }

        void setAttributeLocation(uint32_t _location) {
            attributeDescription.location = _location;
        }

        void setAttributeBinding(uint32_t _binding){
            attributeDescription.binding = _binding;
        }

        VkVertexInputBindingDescription getBindingDescription(){ return bindingDescription;}
        VkVertexInputAttributeDescription getAttributeDescription(){ return attributeDescription;}

        void setBindingLocation(uint32_t binding){
            bindingDescription.binding = binding;
        }

#ifdef CHIBI_DEBUG
        void logData(std::string name);
#endif
        // set the format for the attribute
        void setAttributeFormat(VkFormat format){ attributeDescription.format = format;}

        // sets data into the buffer. Note that this generates it's own usage flags, etc.
        // and does not make use of the usage attribute on the object
        // TODO probably should make it use the attribute on the object.
        template<typename T>
         void setData(vk::VkRendererRef renderer,std::vector<T> data);

        // returns the binding location for the buffer
        uint32_t getBinding(){ return attributeDescription.binding;}

        // returns the shader location for the buffer
        uint32_t getLocation(){ return attributeDescription.location;}

        // returns the format for the Buffer
        VkFormat getFormat(){ return format; }

        // sets the format for the buffer
        void setFormat(VkFormat format){ this->format = format;}

        // sets the expected size for the buffer.
        void setBufferSize(VkDeviceSize size) { bufferSize = size; }

        // sets the usage flags for the buffer.
        void setBufferUsage(VkBufferUsageFlags flags){ usage = flags;}

        // sets the memory flags for the buffer.
        void setMemoryPropertyFlags(VkMemoryPropertyFlags flags){ memoryPropertyFlags = flags;}

        // generates a buffer using object attributes that may have been set.
        void generateBuffer(vk::VkRendererRef renderer);

        // generates a buffer
        void generateBuffer(
                vk::VkRendererRef renderer,
                VkDeviceSize size,
                VkBufferUsageFlags usage,
                VkMemoryPropertyFlags properties,
                VkBuffer& buffer,
                VkDeviceMemory& bufferMemory);

    protected:

        // size of the buffer.
        VkDeviceSize bufferSize;

        // describes how the buffer is utilized.
        VkBufferUsageFlags usage;

        // Vulkan buffer and memory to use.
        VkBuffer buffer;

        // describes memory for the buffer
        VkDeviceMemory bufferMemory;

        // usage flags for the buffer
        VkBufferUsageFlags usageFlags;

        // memory prop flags for the buffer.
        VkMemoryPropertyFlags memoryPropertyFlags;

        // offset at which to start reading the buffer from
        uint32_t descriptionOffset;

        // format for the buffer
        VkFormat format;

        // Vertex binding information for the buffer
        VkVertexInputBindingDescription bindingDescription;

        // attribute information for the buffer
        VkVertexInputAttributeDescription attributeDescription;

        VkVertexInputRate inputRate;

        //////// BUFFER CREATION RELATED ///////////

        // memory flags for buffer
        VkMemoryPropertyFlags memFlags;

        // flags that describe how the buffer will be used.
        VkBufferUsageFlags bufferUsage;

    };

}// end namespace

#endif //CHIBI_BUFFER_H
