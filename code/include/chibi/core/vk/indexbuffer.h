//
// Created by sortofsleepy on 7/19/2020.
//

#ifndef CHIBI_INDEXBUFFER_H
#define CHIBI_INDEXBUFFER_H

#include "chibi/core/vk/buffer.h"


namespace chibi::vk {

    typedef std::shared_ptr<class IndexBuffer> IndexBufferRef;

    class IndexBuffer : public Buffer {

    public:
        IndexBuffer():indexCount(0),indexType(VK_INDEX_TYPE_UINT16){
            usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
        }

        static IndexBufferRef create(){
            return IndexBufferRef(new IndexBuffer);
        }

        template<typename T>
        void loadData(vk::VkRendererRef renderer, std::vector<T> _data){
            auto device = *renderer->getLogicalDevice();

            if(std::is_same<T,uint32_t>::value){
                indexType = VK_INDEX_TYPE_UINT32;
            }

            //setup stride depending on data type for buffer
            bindingDescription.stride = sizeof(T);
            bufferSize = sizeof(_data[0]) * _data.size();
            indexCount = _data.size();

            VkBuffer stagingBuffer;
            VkDeviceMemory stagingBufferMemory;
            generateBuffer(renderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

            // copy data over onto staging buffer
            void* data;
            vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
            memcpy(data, _data.data(), (size_t) bufferSize);
            vkUnmapMemory(device, stagingBufferMemory);

            // generate main buffer to use
            generateBuffer(renderer,bufferSize, bufferUsage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer,bufferMemory);

            // copy info from staging to main buffer.
            bufferutils::copyBuffer(device,*renderer->getGraphicsQueue(),*renderer->getRendererCommandPool(),stagingBuffer,buffer,bufferSize);

            // clean up the staging buffer and memory
            vkDestroyBuffer(*renderer->getLogicalDevice(),stagingBuffer,nullptr);
            vkFreeMemory(*renderer->getLogicalDevice(),stagingBufferMemory,nullptr);
        }

        uint32_t getNumIndices(){
            return indexCount;
        }

        VkIndexType getType(){ return indexType; }
    protected:

        VkIndexType indexType;
        uint32_t indexCount;
    };
}

#endif //CHIBI_INDEXBUFFER_H
