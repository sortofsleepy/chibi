//
// Created by sortofsleepy on 1/17/2020.
//

#ifndef CHIBI_UTILS_H
#define CHIBI_UTILS_H

#include <string>
#include <stdexcept>
#include "chibi/chibi.h"
/**
    Throws a very basic exception message when something goes wrong. Wrapped so we can call the appropriate mechanisms depending on the system.
 */
#ifdef __APPLE__
static void throwException(NSString * instanceMessage,NSString * exceptionName=@"GenericException"){
        NSException * excep = [NSException exceptionWithName:exceptionName reason:instanceMessage userInfo:nil];
            @throw excep;
    }
#else
static void throwException(std::string message){
    throw std::runtime_error(message);
}
#endif

namespace chibi::bufferutils {

    //! Used to copy information between two VkBuffers once.
    static void copyBuffer(VkDevice device,VkQueue queue,VkCommandPool commandPool, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
        VkCommandBufferAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandPool = commandPool;

        allocInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        VkBufferCopy copyRegion{};
        copyRegion.size = size;
        vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        vkFreeCommandBuffers(device,commandPool, 1, &commandBuffer);
    }
}

#endif //CHIBI_UTILS_H
